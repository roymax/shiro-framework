### actions

INSERT INTO `actions` VALUES ('1', '添加', 'post', 'create()', '', '1', '1', '');
INSERT INTO `actions` VALUES ('2', '删除', 'delete', 'destroy()', '', '1', '2', '');
INSERT INTO `actions` VALUES ('4', '修改', 'put', 'edit()', '', '1', '3', '');
INSERT INTO `actions` VALUES ('8', '查询', 'get', 'index()', '', '1', '7', '');

### group 
INSERT INTO `groups` VALUES ('1', '系统管理组', null, '', '', '1', '2', '');


### groups_roles
INSERT INTO `groups_roles` VALUES ('17', '1', '1');


### Resoures
INSERT INTO `resources` VALUES ('5', '系统中心', 'home','/home', '1', null, '', '',  '1', '4', '');
INSERT INTO `resources` VALUES ('8', '人员管理', 'users', '/user/' ,'1', '5', '',  '1', '7', '');
INSERT INTO `resources` VALUES ('10', '组管理', 'group','/group/' , '1', '5', '',  '1', '9', '饿');
INSERT INTO `resources` VALUES ('11', '角色管理', 'role','/group/' , '1', '5', '', 'leaf100', '1', '8', '');
INSERT INTO `resources` VALUES ('21', '资源信息', 'resource', '/group/' ,'1', '5', '',  '1', '13', '');
INSERT INTO `resources` VALUES ('22', '系统信息', 'system','/group/' , '1', '5', '', '1', '19', '');

### roles
INSERT INTO `role` VALUES ('1', 'administrator', '', '1', '1', '系统管理员');


### roles_auths
INSERT INTO `roles_auths` VALUES ('238', '1', '5', 'home:get');
-- INSERT INTO `roles_auths` VALUES ('239', '21', '33', null);
-- INSERT INTO `roles_auths` VALUES ('240', '21', '49', null);
-- INSERT INTO `roles_auths` VALUES ('241', '21', '50', null);
-- INSERT INTO `roles_auths` VALUES ('242', '21', '5', null);
-- INSERT INTO `roles_auths` VALUES ('243', '21', '26', null);
-- INSERT INTO `roles_auths` VALUES ('244', '21', '18', null);
-- INSERT INTO `roles_auths` VALUES ('245', '21', '10', null);
-- INSERT INTO `roles_auths` VALUES ('246', '21', '42', null);

### systems
INSERT INTO `systems` VALUES ('1', '测试系统', 'test', '/', '', 'test', '', 'v1.0', '', '1', '6', '测试信息');

### users
INSERT INTO `users` VALUES ('2', 'test', 'test', '测试用户', 'test', '1', '工人', '', '23', '', 'sex100', 'marriage100', 'education101', '山东青岛', '山东青岛', '13436521458', 'test@163.com', '', '', '', '', '1', '', '2112', '');
INSERT INTO `users` VALUES ('1', 'admin', 'admin', 'admin', 'admin', '1', 'admin', '', '', '', 'sex100', 'marriage100', 'education100', '', '', '', '', '', '', '', '', '1', '', '', null);

### users_groups
INSERT INTO `users_groups` VALUES ('1', '1');
INSERT INTO `users_groups` VALUES ('2', '1');
