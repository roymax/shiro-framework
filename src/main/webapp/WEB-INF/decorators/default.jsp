<!DOCTYPE html>
<%@ page language="java" pageEncoding="UTF-8" %>
<%request.setAttribute("ctx", request.getContextPath());%>

<%@include file="/WEB-INF/content/inc/taglib.jsp" %>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><decorator:title default="管理后台"/> - <bo:config key="name"/></title>
    <meta name="description" content="<bo:config key="name"/>">
    <meta name="keywords" content="<bo:config key="name"/>">

    <%@ include file="/WEB-INF/content/inc/import.jsp" %>

    <decorator:head/>

</head>

<body
        <decorator:getProperty property="body.id" writeEntireProperty="true"/> <decorator:getProperty
        property="body.class"
        writeEntireProperty="true"/>>

<div class="container-fluid">
    <div class="row-fluid">
        <decorator:body/>
    </div>
</div>

</body>

</html>
