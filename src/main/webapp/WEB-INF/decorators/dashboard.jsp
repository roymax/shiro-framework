<!DOCTYPE html>
<%@ page language="java" pageEncoding="UTF-8" %>
<%request.setAttribute("ctx", request.getContextPath());%>

<%@ include file="/WEB-INF/content/inc/taglib.jsp" %>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="chrome=1">

    <title><decorator:title default="管理后台"/> - <bo:config key="name"/></title>
    <meta name="description" content="<bo:config key="name"/>">
    <meta name="keywords" content="<bo:config key="name"/>">

    <%@ include file="/WEB-INF/content/inc/import.jsp" %>

    <decorator:head/>

</head>

<body
        <decorator:getProperty property="body.id" writeEntireProperty="true"/> <decorator:getProperty
        property="body.class"
        writeEntireProperty="true"/>>
<%@ include file="/WEB-INF/content/inc/menu.jsp" %>
<div class="container-fluid">

    <div class="row-fluid">
        <div class="span2">
            <jsp:include page="/WEB-INF/content/inc/sidebar-nav.jsp" />
        </div>
        <div class="span10">
            <div>
                <jsp:include page="/WEB-INF/content/inc/breadcrumb.jsp" />
            </div>
            <div class="row-fluid">
                <decorator:body/>
            </div>

        </div>
    </div>
    <%@ include file="/WEB-INF/content/inc/footer.jsp" %>

    <div class="modal hide fade" id="delModal">
      <div class="modal-body">
        <p>One fine body…</p>
      </div>
      <div class="modal-footer">
          <a href="#" class="btn btn-danger">删除</a>
          <a href="#" class="btn" data-dismiss="modal">取消</a>
      </div>
    </div>
</div>

</body>

</html>
