<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="sidebar-nav">
    <div class="well" style="padding: 8px 0;">
        <ul class="nav nav-list">
            <li><a href="${ctx}/dashboard/"><i class="icon-home"></i> Dashboard</a></li>
            <li class="nav-header">系统管理</li>
                <li><a href="${ctx}/security/resource"><i class="icon-edit"></i> 资源管理</a></li>
                <li><a href="${ctx}/security/role"><i class="icon-user"></i> 角色管理</a></li>
                <li><a href="${ctx}/security/group"><i class="icon-comment"></i> 群组管理</a></li>
                <li><a href="${ctx}/security/user"><i class="icon-picture"></i> 用户管理</a></li>

            <li class="nav-header">Typography</li>
            <li><a href="typography.html"><i class="icon-font"></i> Typography</a></li>
            <li><a href="grid.html"><i class="icon-th-large"></i> Grid</a></li>
            <li><a href="portlets.html"><i class="icon-th"></i> Portlets</a></li>
            <li><a href="forms.html"><i class="icon-th"></i> Forms</a></li>
            <li class="active"><a href="tables.html"><i class="icon-align-justify"></i> Tables</a></li>
            <li><a href="other.html"><i class="icon-gift"></i> Other</a></li>
            <li class="nav-header">Settings</li>
            <li><a class="cookie-delete" href="#"><i class="icon-wrench"></i> Delete Cookies</a></li>
            <li><a class="sidenav-style-1" href="#"><i class="icon-align-left"></i> Side Menu Style 1</a>
            </li>
            <li><a class="sidenav-style-2" href="#"><i class="icon-align-right"></i> Side Menu Style 2</a>
            </li>
            <li><a href="login.html"><i class="icon-off"></i> Logout</a></li>
        </ul>
    </div>
</div>