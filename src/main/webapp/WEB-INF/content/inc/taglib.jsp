<%@ taglib uri = "http://www.opensymphony.com/sitemesh/decorator"  prefix = "decorator" %> 
<%@ taglib uri = "http://www.opensymphony.com/sitemesh/page"  prefix = "page" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="bo" uri="http://gzbosoft.com/framework" %>