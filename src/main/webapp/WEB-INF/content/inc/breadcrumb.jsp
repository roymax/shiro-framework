<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i> <a href="${ctx}/dashboard">主工作台</a> <span class="divider">/</span>
    </li>
    <li>
        <a href="grid.html">系统管理</a> <span class="divider">/</span>
    </li>
    <li class="active">资源管理</li>
</ul>