    <link href="<c:url value='/public/css/bootstrap-cerulean.min.css'/>" rel="stylesheet">
    <%--<link href="<c:url value='/public/css/bootstrap-spacelab.min.css'/>" rel="stylesheet">--%>
    <link href="<c:url value='/public/css/bootstrap-responsive.min.css'/>" rel="stylesheet">
    <link href="<c:url value='/public/css/style.css'/>" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="<c:url value='/public/js/bootstrap/html5.js'/>"></script>
    <![endif]-->

    <script type="text/javascript" src="<c:url value='/public/js/jquery/jquery.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/public/js/bootstrap/bootstrap.min.js'/>"></script>

    <script type="text/javascript" src="<c:url value='/public/js/application.js'/>"></script>