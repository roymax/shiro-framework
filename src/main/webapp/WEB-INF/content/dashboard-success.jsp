<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@include file="/WEB-INF/content/inc/taglib.jsp" %>
<html decorator="dashboard">
<head>
    <meta name="decorator" content="dashboard">
    <title>我的工作台</title>
</head>

<body>

<!-- Portlet Set 4 -->
<div class="span12">
<!-- Portlet: Data Table -->
<div class="box">


<div class="box-container-toggle">
<div class="box-content">
<div id="datatable_wrapper" class="dataTables_wrapper" role="grid">
<div class="row-fluid">
    <div class="span6">
        <div id="datatable_length" class="dataTables_length"><label><select size="1" name="datatable_length"
                                                                            aria-controls="datatable">
            <option value="10" selected="selected">10</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="100">100</option>
        </select> records per page</label></div>
    </div>
    <div class="span6">
        <div class="dataTables_filter" id="datatable_filter"><label>Search: <input type="text"
                                                                                   aria-controls="datatable"></label>
        </div>
    </div>
</div>
<table cellpadding="0" cellspacing="0" border="0"
       class="table table-striped table-bordered bootstrap-datatable dataTable" id="datatable"
       aria-describedby="datatable_info">
<thead>
<tr role="row">
    <th class="header headerSortDown" role="columnheader" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1"
        style="width: 177px; " aria-sort="ascending" aria-label="Username: activate to sort column descending">Username
    </th>
    <th class="header" role="columnheader" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1"
        style="width: 172px; " aria-label="Date registered: activate to sort column ascending">Date registered
    </th>
    <th class="header" role="columnheader" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1"
        style="width: 93px; " aria-label="Role: activate to sort column ascending">Role
    </th>
    <th class="header" role="columnheader" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1"
        style="width: 99px; " aria-label="Status: activate to sort column ascending">Status
    </th>
    <th class="header" role="columnheader" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1"
        style="width: 379px; " aria-label="Actions: activate to sort column ascending">Actions
    </th>
</tr>
</thead>

<tbody role="alert" aria-live="polite" aria-relevant="all">
<tr class="odd">
    <td class="  sorting_1">Bananaman</td>
    <td class="center ">2011/06/01</td>
    <td class="center ">Admin</td>
    <td class="center ">
        <span class="label">Inactive</span>
    </td>
    <td class="center ">
        <a class="btn btn-success" href="#">
            <i class="icon-zoom-in icon-white"></i>
            View
        </a>
        <a class="btn btn-info" href="#">
            <i class="icon-edit icon-white"></i>
            Edit
        </a>
        <a class="btn btn-danger" href="#">
            <i class="icon-trash icon-white"></i>
            Delete
        </a>
    </td>
</tr>
<tr class="even">
    <td class="  sorting_1">Batman</td>
    <td class="center ">2011/02/01</td>
    <td class="center ">Admin</td>
    <td class="center ">
        <span class="label">Inactive</span>
    </td>
    <td class="center ">
        <a class="btn btn-success" href="#">
            <i class="icon-zoom-in icon-white"></i>
            View
        </a>
        <a class="btn btn-info" href="#">
            <i class="icon-edit icon-white"></i>
            Edit
        </a>
        <a class="btn btn-danger" href="#">
            <i class="icon-trash icon-white"></i>
            Delete
        </a>
    </td>
</tr>
<tr class="odd">
    <td class="  sorting_1">Bob the Builder</td>
    <td class="center ">2011/02/01</td>
    <td class="center ">Admin</td>
    <td class="center ">
        <span class="label">Inactive</span>
    </td>
    <td class="center ">
        <a class="btn btn-success" href="#">
            <i class="icon-zoom-in icon-white"></i>
            View
        </a>
        <a class="btn btn-info" href="#">
            <i class="icon-edit icon-white"></i>
            Edit
        </a>
        <a class="btn btn-danger" href="#">
            <i class="icon-trash icon-white"></i>
            Delete
        </a>
    </td>
</tr>
<tr class="even">
    <td class="  sorting_1">Catwomen</td>
    <td class="center ">2010/01/21</td>
    <td class="center ">Staff</td>
    <td class="center ">
        <span class="label label-success">Active</span>
    </td>
    <td class="center ">
        <a class="btn btn-success" href="#">
            <i class="icon-zoom-in icon-white"></i>
            View
        </a>
        <a class="btn btn-info" href="#">
            <i class="icon-edit icon-white"></i>
            Edit
        </a>
        <a class="btn btn-danger" href="#">
            <i class="icon-trash icon-white"></i>
            Delete
        </a>
    </td>
</tr>
<tr class="odd">
    <td class="  sorting_1">Fred Flinstone</td>
    <td class="center ">2011/01/01</td>
    <td class="center ">Member</td>
    <td class="center ">
        <span class="label label-success">Active</span>
    </td>
    <td class="center ">
        <a class="btn btn-success" href="#" rel="tooltip" data-original-title="View">
            <i class="icon-zoom-in icon-white"></i>
            View
        </a>
        <a class="btn btn-info" href="#" rel="tooltip" data-original-title="Edit">
            <i class="icon-edit icon-white"></i>
            Edit
        </a>
        <a class="btn btn-danger" href="#" rel="tooltip" data-original-title="Delete">
            <i class="icon-trash icon-white"></i>
            Delete
        </a>
    </td>
</tr>
<tr class="even">
    <td class="  sorting_1">Garfield</td>
    <td class="center ">2011/08/23</td>
    <td class="center ">Staff</td>
    <td class="center ">
        <span class="label label-important">Banned</span>
    </td>
    <td class="center ">
        <a class="btn btn-success" href="#">
            <i class="icon-zoom-in icon-white"></i>
            View
        </a>
        <a class="btn btn-info" href="#">
            <i class="icon-edit icon-white"></i>
            Edit
        </a>
        <a class="btn btn-danger" href="#">
            <i class="icon-trash icon-white"></i>
            Delete
        </a>
    </td>
</tr>
<tr class="odd">
    <td class="  sorting_1">Hulk</td>
    <td class="center ">2011/02/01</td>
    <td class="center ">Staff</td>
    <td class="center ">
        <span class="label label-important">Banned</span>
    </td>
    <td class="center ">
        <a class="btn btn-success" href="#">
            <i class="icon-zoom-in icon-white"></i>
            View
        </a>
        <a class="btn btn-info" href="#">
            <i class="icon-edit icon-white"></i>
            Edit
        </a>
        <a class="btn btn-danger" href="#">
            <i class="icon-trash icon-white"></i>
            Delete
        </a>
    </td>
</tr>
<tr class="even">
    <td class="  sorting_1">MacAndCheese</td>
    <td class="center ">2011/03/01</td>
    <td class="center ">Member</td>
    <td class="center ">
        <span class="label label-warning">Pending</span>
    </td>
    <td class="center ">
        <a class="btn btn-success" href="#">
            <i class="icon-zoom-in icon-white"></i>
            View
        </a>
        <a class="btn btn-info" href="#">
            <i class="icon-edit icon-white"></i>
            Edit
        </a>
        <a class="btn btn-danger" href="#">
            <i class="icon-trash icon-white"></i>
            Delete
        </a>
    </td>
</tr>
<tr class="odd">
    <td class="  sorting_1">Paul</td>
    <td class="center ">2011/03/01</td>
    <td class="center ">Member</td>
    <td class="center ">
        <span class="label label-warning">Pending</span>
    </td>
    <td class="center ">
        <a class="btn btn-success" href="#">
            <i class="icon-zoom-in icon-white"></i>
            View
        </a>
        <a class="btn btn-info" href="#">
            <i class="icon-edit icon-white"></i>
            Edit
        </a>
        <a class="btn btn-danger" href="#">
            <i class="icon-trash icon-white"></i>
            Delete
        </a>
    </td>
</tr>
<tr class="even">
    <td class="  sorting_1">Ratamatat</td>
    <td class="center ">2010/01/21</td>
    <td class="center ">Staff</td>
    <td class="center ">
        <span class="label label-success">Active</span>
    </td>
    <td class="center ">
        <a class="btn btn-success" href="#">
            <i class="icon-zoom-in icon-white"></i>
            View
        </a>
        <a class="btn btn-info" href="#">
            <i class="icon-edit icon-white"></i>
            Edit
        </a>
        <a class="btn btn-danger" href="#">
            <i class="icon-trash icon-white"></i>
            Delete
        </a>
    </td>
</tr>
</tbody>
</table>
<div class="row-fluid">
    <div class="span6">
        <div class="dataTables_info" id="datatable_info">Showing 1 to 10 of 16 entries</div>
    </div>
    <div class="span6">
        <div class="dataTables_paginate paging_bootstrap pagination">
            <ul>
                <li class="prev disabled"><a href="#">← Previous</a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li class="next"><a href="#">Next → </a></li>
            </ul>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
<!--/span-->
</div>


</body>
</html>

