<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@include file="/WEB-INF/content/inc/taglib.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>资源管理</title>
</head>
<body>
<div class="alert alert-error">
    <a class="close" data-dismiss="alert" href="#">×</a>
    <strong>Oh snap!</strong> Something went wrong.
</div>
<div class="alert alert-success">
    <a class="close" data-dismiss="alert" href="#">×</a>
    <strong>Well done!</strong> That went as it should.
</div>

<div class="page-header">
    <h1>资源管理</h1>
</div>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${ctx}/security/resource/new">
            <div><i class="icon-pencil"></i></div>
            <div><strong>创建</strong></div>
        </a>
    </div>

</div>
<div>
    <table class="table table-striped table-bordered bootstrap-datatable dataTable">
        <thead>
        <tr>
            <th>id</th>
            <th>中文名称</th>
            <th>权限代码</th>
            <th>状态</th>
            <th>操作</th>
        </tr>
        </thead>

        <tbody>
        <c:forEach var="item" items="${paging.result}">
            <tr>
                <td>${item.id}</td>
                <td>${item.name}</td>
                <td>${item.code}</td>
                <td>${item.status eq '1' ? '<span class="label label-success">有效</span>' : '<span class="label">无效</span>'}
                </td>
                <td class="center ">
                    <a class="btn btn-success" href="#">
                        <i class="icon-zoom-in icon-white"></i>
                        查看
                    </a>
                    <a class="btn btn-info" href="#">
                        <i class="icon-edit icon-white"></i>
                        编辑
                    </a>
                    <a class="btn btn-danger" data-toggle="modal" href="#delModal"
                       data-id="${ctx}/security/resource/${item.id}">
                        <i class="icon-trash icon-white"></i>
                        删除
                    </a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    $(function () {
        $('#delModal').bind('show', function () {
            var action = $(this).data('id'),
                    removeBtn = $(this).find('.btn-danger'),
                    href = removeBtn.attr('href');
            console.log("action is " + action);
            removeBtn.attr('href', action);
        });
    });


    function del(uri) {
        $.post(uri + ".json?_method=delete", function (data) {
            alert(data);
        });
    }
</script>
</body>
</html>