<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@include file="/WEB-INF/content/inc/taglib.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>资源管理-创建</title>
</head>
<body>
<form class="form-horizontal" method="post" action="${ctx}/security/resource/">
    <fieldset>
        <legend>新增资源</legend>
        <div class="control-group">
            <label class="control-label" for="name">资源名称</label>

            <div class="controls">
                <input type="text" class="span6" id="name" name="name">

                <p class="help-block">请输入资源名称，如：新增角色</p>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="code">资源代码</label>

            <div class="controls">
                <input type="text" class="span6" id="code" name="code">

                <p class="help-block">请输入资源代码，如：resource:new</p>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="select01">所属系统</label>

            <div class="controls">
                <select id="select01" class="chzn-select chzn-done" name="system.id">
                    <option value="1">1</option>
                </select>
            </div>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn btn-primary">保存</button>
            <button type="reset" class="btn">重置</button>
        </div>
    </fieldset>
</form>

</body>
</html>