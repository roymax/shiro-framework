<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@include file="/WEB-INF/content/inc/taglib.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="decorator" content="default">
    <title>欢迎您，请登录</title>
</head>
<body>


<div id="login-page" class="row-fluid">
    <div class="span4"></div>
    <div class="span4">
        <div class="page-header">
            <h1><bo:config key="name"/></h1>
          </div>
        <form id="login-form" class="well" action="<c:url value="/security/login" />" method="POST">
            <input type="text" class="input-large" placeholder="用户名" id="username" name="username"/><br/>
            <input type="password" class="input-large" placeholder="密码" id="password" name="password"/><br/>

            <button type="submit" class="btn btn-primary">登录</button>
            <%--<button type="submit" class="btn">Forgot Password</button>--%>
        </form>
    </div>
</div>
<%@ include file="/WEB-INF/content/inc/footer.jsp" %>

</body>
</html>