<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@include file="/WEB-INF/content/inc/taglib.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>资源管理</title>
</head>
<body>

<div class="page-header">
<h1>资源管理</h1>
    <span class="pull-right">
        aaaaaaa

    </span>
</div>
<div>
    <table class="table table-striped table-bordered bootstrap-datatable dataTable">
        <thead>
        <tr>
            <th>id</th>
            <th>中文名称</th>
            <th>权限代码</th>
            <th>操作</th>
        </tr>
        </thead>

        <tbody>
        <c:forEach var="item" items="${paging.result}">
            <tr>
                <td>${item.id}</td>
                <td>${item.name}</td>
                <td>${item.code}</td>
                <td></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

</body>
</html>