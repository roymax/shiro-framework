/**
 *
 */
package com.gzbosoft.framework.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DateUtils {

	// ~ Static fields/initializers
	// =============================================

	private static Log log = LogFactory.getLog(DateUtils.class);

	private static String defaultDatePattern = "yyyy-MM-dd";

	private static String timePattern = "yyyy-MM-dd HH:mm";

	// ~ Methods
	// ================================================================

	public static String getDatePattern() {
		return defaultDatePattern;
	}

	public static String getTimePattern() {
		return timePattern;
	}

	/**
	 * 
	 * 转换为yyyy-MM-dd格式的字符串输出
	 * 
	 * @param aDate
	 *            date from database as a string
	 * @return formatted string for the ui
	 */
	public static final String getDate(Date aDate) {
		SimpleDateFormat df = null;
		String returnValue = "";

		if (aDate != null) {
			df = new SimpleDateFormat(getDatePattern());
			returnValue = df.format(aDate);
		}

		return (returnValue);
	}

	/**
	 * 
	 * 转换为固定格式的字符串输出
	 * 
	 * @param aDate
	 *            date from database as a string
	 * @return formatted string for the ui
	 */
	public static final String getDate(Date aDate, String format) {
		SimpleDateFormat df = null;
		String returnValue = "";

		if (aDate != null) {
			df = new SimpleDateFormat(format);
			returnValue = df.format(aDate);
		}

		return (returnValue);
	}

	/**
	 * This method generates a string representation of a date/time in the
	 * format you specify on input
	 * 
	 * @param aMask
	 *            the date pattern the string is in
	 * @param strDate
	 *            a string representation of a date
	 * @return a converted Date object
	 * @see java.text.SimpleDateFormat
	 * @throws java.text.ParseException
	 */
	public static final Date convertStringToDate(String aMask, String strDate)
			throws ParseException {
		SimpleDateFormat df = null;
		Date date = null;
		df = new SimpleDateFormat(aMask);

		// if (log.isDebugEnabled()) {
		// log.debug("converting '" + strDate + "' to date with mask '" + aMask
		// + "'");
		// }

		try {
			date = df.parse(strDate);
		} catch (ParseException pe) {
			// log.error("ParseException: " + pe);
			throw new ParseException(pe.getMessage(), pe.getErrorOffset());
		}

		return (date);
	}

	/**
	 * This method returns the current date time in the format: MM/dd/yyyy HH:MM
	 * a
	 * 
	 * @param theTime
	 *            the current time
	 * @return the current date/time
	 */
	public static String getTimeNow(Date theTime) {
		return getDateTime(timePattern, theTime);
	}

	/**
	 * This method returns the current date in the format: MM/dd/yyyy
	 * 
	 * @return the current date
	 * @throws java.text.ParseException
	 */
	public static Calendar getToday() throws ParseException {
		Date today = new Date();
		SimpleDateFormat df = new SimpleDateFormat(getDatePattern());

		// This seems like quite a hack (date -> string -> date),
		// but it works ;-)
		String todayAsString = df.format(today);
		Calendar cal = new GregorianCalendar();
		cal.setTime(convertStringToDate(todayAsString));

		return cal;
	}

	/**
	 * This method generates a string representation of a date's date/time in
	 * the format you specify on input
	 * 
	 * @param aMask
	 *            the date pattern the string is in
	 * @param aDate
	 *            a date object
	 * @return a formatted string representation of the date
	 * 
	 * @see java.text.SimpleDateFormat
	 */
	public static final String getDateTime(String aMask, Date aDate) {
		SimpleDateFormat df = null;
		String returnValue = "";

		if (aDate == null) {
			if (log.isErrorEnabled()) {
				log.error("aDate is null!");
			}
		} else {
			df = new SimpleDateFormat(aMask);
			returnValue = df.format(aDate);
		}

		return (returnValue);
	}

	/**
	 * This method generates a string representation of a date based on the
	 * System Property 'dateFormat' in the format you specify on input
	 * 
	 * @param aDate
	 *            A date to convert
	 * @return a string representation of the date
	 */
	public static final String convertDateToString(Date aDate) {
		return getDateTime(getDatePattern(), aDate);
	}

	/**
	 * This method converts a String to a date using the datePattern
	 * 
	 * @param strDate
	 *            the date to convert (in format MM/dd/yyyy)
	 * @return a date object
	 * 
	 * @throws java.text.ParseException
	 */
	public static Date convertStringToDate(String strDate)
			throws ParseException {
		Date aDate = null;

		try {
			// if (log.isDebugEnabled()) {
			// log.debug("converting date with pattern: " + getDatePattern());
			// }

			aDate = convertStringToDate(getDatePattern(), strDate);
		} catch (ParseException pe) {
			if (log.isErrorEnabled()) {
				log.error("Could not convert '" + strDate
						+ "' to a date, throwing exception");
			}
			throw new ParseException(pe.getMessage(), pe.getErrorOffset());

		}

		return aDate;
	}

	/**
	 * 返回开始日期格式,为 yyyy-MM-dd 00:00:00
	 * 
	 * @param date
	 *            如果输入null,则取当天日期
	 * @return yyyy-MM-dd 00:00:00格式的日期
	 */

	public static Date getStartDate(Date date) {
		if (date == null) {
			date = new Date(System.currentTimeMillis());
		}
		Calendar rightNow = Calendar.getInstance();
		rightNow.setTime(date);
		rightNow.set(Calendar.HOUR_OF_DAY, 0);
		rightNow.set(Calendar.MINUTE, 0);
		rightNow.set(Calendar.SECOND, 0);

		return rightNow.getTime();
	}

	/**
	 * 返回开始日期格式,为 yyyy-MM-dd 23:59:59
	 * 
	 * @param date
	 *            如果输入null,则取当天日期
	 * @return yyyy-MM-dd 23:59:59格式的日期
	 */

	public static Date getEndDate(Date date) {
		if (date == null) {
			date = new Date(System.currentTimeMillis());
		}
		Calendar rightNow = Calendar.getInstance();
		rightNow.setTime(date);
		rightNow.set(Calendar.HOUR_OF_DAY, 23);
		rightNow.set(Calendar.MINUTE, 59);
		rightNow.set(Calendar.SECOND, 59);

		return rightNow.getTime();
	}

	/**
	 * 返回开始日期格式,为 yyyy-MM-dd 00:00:00
	 * 
	 * @param date
	 *            如果输入null,则取当天日期
	 * @return yyyy-MM-dd 00:00:00格式的日期
	 */

	public static Calendar getStartCalendar(Date date) {
		if (date == null) {
			date = new Date(System.currentTimeMillis());
		}
		Calendar rightNow = Calendar.getInstance();
		rightNow.setTime(date);
		rightNow.set(Calendar.HOUR_OF_DAY, 0);
		rightNow.set(Calendar.MINUTE, 0);
		rightNow.set(Calendar.SECOND, 0);

		return rightNow;
	}

	/**
	 * 返回开始日期格式,为 yyyy-MM-dd 23:59:59
	 * 
	 * @param date
	 *            如果输入null,则取当天日期
	 * @return yyyy-MM-dd 23:59:59格式的日期
	 */

	public static Calendar getEndCalendar(Date date) {
		if (date == null) {
			date = new Date(System.currentTimeMillis());
		}
		Calendar rightNow = Calendar.getInstance();
		rightNow.setTime(date);
		rightNow.set(Calendar.HOUR_OF_DAY, 23);
		rightNow.set(Calendar.MINUTE, 59);
		rightNow.set(Calendar.SECOND, 59);

		return rightNow;
	}

	/**
	 * 返回开始日期格式,为 yyyy-MM-dd 00:00:00
	 * 
	 * @param date
	 *            如果输入null,则取当天日期
	 * @return yyyy-MM-dd 00:00:00格式的日期
	 */

	public static Date getStartStringToDate(String sdate) {
		Date date = null;
		if (sdate != null && !sdate.equals("")) {
			try {
				date = convertStringToDate("yyyy-MM-dd", sdate);
			} catch (ParseException e) {
				if (log.isWarnEnabled()) {
					log.warn("字符串转换为日期类型出错:" + sdate, e);
				}
				return date;
			}
		} else
			return null;
		Calendar rightNow = Calendar.getInstance();
		rightNow.setTime(date);
		rightNow.set(Calendar.HOUR_OF_DAY, 0);
		rightNow.set(Calendar.MINUTE, 0);
		rightNow.set(Calendar.SECOND, 0);

		return rightNow.getTime();
	}

	/**
	 * 返回开始日期格式,为 yyyy-MM-dd 23:59:59
	 * 
	 * @param date
	 *            如果输入null,则取当天日期
	 * @return yyyy-MM-dd 23:59:59格式的日期
	 */

	public static Date getEndStringToDate(String sdate) {
		Date date = null;
		if (sdate != null && !sdate.equals("")) {
			try {
				date = convertStringToDate("yyyy-MM-dd", sdate);
			} catch (ParseException e) {
				if (log.isWarnEnabled()) {
					log.warn("字符串转换为日期类型出错:" + sdate, e);
				}
				return date;
			}
		} else
			return null;
		Calendar rightNow = Calendar.getInstance();
		rightNow.setTime(date);
		rightNow.set(Calendar.HOUR_OF_DAY, 23);
		rightNow.set(Calendar.MINUTE, 59);
		rightNow.set(Calendar.SECOND, 59);

		return rightNow.getTime();
	}

	/**
	 * 取得输入日期字符串的正负N天对应的日期,如果输入的日期为null或者""则取当前日期
	 * 
	 * @author Nigel Create Time: 2005-12-15
	 * @param dateStr
	 *            日期字符串
	 * @param before
	 * @return
	 */
	public static String getDateBefore(String dateStr, int before) {
		Date date = null;
		if (dateStr != null && !dateStr.equals("")) {
			try {
				date = convertStringToDate("yyyy-MM-dd", dateStr);
			} catch (ParseException e) {
				if (log.isWarnEnabled()) {
					log.warn("字符串转换为日期类型出错:" + dateStr, e);
				}
				return "";
			}
		}
		Calendar rightNow = Calendar.getInstance();
		if (date != null)
			rightNow.setTime(date);
		rightNow.set(Calendar.DATE, rightNow.get(Calendar.DATE) + before);
		return getDate(rightNow.getTime());
	}

	/**
	 * 日期比较 Create Time: 2005-12-15
	 * 
	 * @param date1
	 *            (格式：yyyy-mm-dd)
	 * @param date2
	 *            (格式：yyyy-mm-dd)
	 * @return 相差天数
	 */
	public static long getDateCompare(String date1, String date2) {

		String[] strDate = { date1, date2 };
		int iLen = strDate.length;

		int[] slashMarkIndex1 = new int[iLen];
		int[] slashMarkIndex2 = new int[iLen];

		String[] year = new String[iLen];
		String[] month = new String[iLen];
		String[] day = new String[iLen];
		int[] iYear = new int[iLen];
		int[] iMonth = new int[iLen];
		int[] iDay = new int[iLen];

		Calendar[] calen = new Calendar[iLen];
		Date[] dTime = new Date[iLen];

		for (int i = 0; i < strDate.length; i++) {

			slashMarkIndex1[i] = strDate[i].indexOf("-");
			slashMarkIndex2[i] = strDate[i]
					.indexOf("-", slashMarkIndex1[i] + 1);

			year[i] = strDate[i].substring(0, slashMarkIndex1[i]);
			month[i] = strDate[i].substring(slashMarkIndex1[i] + 1,
					slashMarkIndex2[i]);
			day[i] = strDate[i].substring(slashMarkIndex2[i] + 1);

			iYear[i] = Integer.parseInt(year[i]);
			iMonth[i] = Integer.parseInt(month[i]) - 1;
			iDay[i] = Integer.parseInt(day[i]);

			calen[i] = Calendar.getInstance();

			calen[i].clear();

			calen[i].set(iYear[i], iMonth[i], iDay[i]);
			dTime[i] = calen[i].getTime();
		}

		long diff = dTime[0].getTime() - dTime[1].getTime();

		return (diff / 1000 / 60 / 60 / 24);
	}

	/**
	 * Chinese date time formate
	 */
	public static final String CHINESE_DATE_TIME = "yyyy年MM月dd日 HH:mm";

	/**
	 * Chinese date format
	 */
	public static final String CHINESE_DATE = "yyyy年MM月dd日";

	/**
	 * Default date time format
	 */
	public static final String DEFAULT_DATE_TIME = "yyyy-MM-dd HH:mm:ss";

	/**
	 * Default date time format
	 */
	public static final String DEFAULT_DATE_TIME_SECOND = "yyyy-MM-dd HH:mm";

	/**
	 * Default date format
	 */
	public static final String DEFAULT_DATE = "yyyy-MM-dd";

	/**
	 * Default time format
	 */
	public static final String DEFAULT_TIME = "HH:mm:ss";

	/**
	 * For log and file's date format
	 */
	public static final String LOG_DATE_TIME = "yyyyMMddHHmmssSSS";

	/**
	 * 一天的表示形式
	 */
	public static final long DURATION = 1000L * 60 * 60 * 24;// 一天

	/**
	 * Count the days between 2 date,if the first date argument is before the
	 * second one, a negative number wiil be returned
	 * 
	 * @param date1
	 *            A <code>java.util.Date</code> object
	 * @param date2
	 *            A <code>java.util.Date</code> object
	 * @return Day counts between the two input day If the first date argument
	 *         is before the seconde one, a negative number will be returned.
	 */
	public static int daysBetween(Date date1, Date date2) {
		if (date1 == null || date2 == null)
			return 0;
		GregorianCalendar cal1 = new GregorianCalendar();
		cal1.setTime(date1);
		GregorianCalendar cal2 = new GregorianCalendar();
		cal2.setTime(date2);
		int before = cal1.compareTo(cal2);
		// make the later one always in cal1
		if (before < 0) {
			cal1.setTime(date2);
			cal2.setTime(date1);

		}
		int day1 = cal1.get(Calendar.DAY_OF_YEAR);
		int day2 = cal2.get(Calendar.DAY_OF_YEAR);
		int year1 = cal1.get(Calendar.YEAR);
		int year2 = cal2.get(Calendar.YEAR);
		int yearBetween = year1 - year2;
		for (int i = 0; i < yearBetween; i++) {
			cal2.set(Calendar.YEAR, year2 + i);
			day1 = day1 + cal2.getActualMaximum(Calendar.DAY_OF_YEAR);
		}
		return (day1 - day2) * before;
	}

	/**
	 * 计算两个日期的时间差 返回具体的天数
	 * 
	 * @param compareTime
	 * @param currentTime
	 * @return int
	 */
	public static int hoursBetween(Date compareTime, Date currentTime) {
		if (compareTime == null || currentTime == null)
			return 0;
		return (int) ((compareTime.getTime() - currentTime.getTime()) / DURATION);
	}

	/**
	 * 计算两个时间之间相差的分钟数，“结束时间”减去“开始时间”之间的分钟数.
	 * 
	 * @param beginTime
	 *            开始时间
	 * @param endTime
	 *            结束时间
	 * @return int 分钟数。如果开始时间或者结束时间为 null 的话，返回 0 。
	 */
	public static int minutesBetween(Date beginTime, Date endTime) {
		if (beginTime == null || endTime == null) {
			return 0;
		}
		long diff = endTime.getTime() - beginTime.getTime();
		return (int) (diff / (60 * 1000));
	}

	/**
	 * 计算两个日期的时间差 返回具体的天数
	 * 
	 * @param compareTime
	 * @param currentTime
	 * @return int
	 */
	public static int secondsBetween(Date compareTime, Date currentTime) {
		if (compareTime == null || currentTime == null)
			return 0;
		return (int) ((compareTime.getTime() - currentTime.getTime()) / 1000L);
	}

	/**
	 * Change a Date into a birthday to today
	 * 
	 * @param birthDay
	 *            A birthday date
	 * @return
	 */
	public static int getAge(Object birthDay) {
		Date aDate = null;
		// if(birthDay instanceof DATE){
		// DATE date = (DATE)birthDay;
		// aDate = date.dateValue();
		// }else if(birthDay instanceof Date){
		if (birthDay instanceof Date) {
			aDate = (Date) birthDay;
		}
		if (aDate != null) {
			Calendar cc = new GregorianCalendar();
			Calendar bc = new GregorianCalendar();
			bc.setTime(aDate);
			int bYear = cc.get(Calendar.YEAR) - bc.get(Calendar.YEAR);
			int bDay = cc.get(Calendar.DAY_OF_YEAR)
					- bc.get(Calendar.DAY_OF_YEAR);
			// not yet match 1 round age, minus one year
			if (bDay < 0)
				bYear = bYear - 1;
			return bYear;
		}
		return 0;
	}

	/**
	 * Get birthday date according to the age
	 * 
	 * @param age
	 *            Age number
	 * @return The birthdate
	 */
	public static Date getBirthDay(int age) {
		GregorianCalendar gc = new GregorianCalendar();
		int currYear = gc.get(Calendar.YEAR);
		gc.set(Calendar.YEAR, currYear - age);
		Date birthDate = new Date(gc.getTime().getTime());// 精确到天，haidong.zhou
		// edit 2006-07-14
		return birthDate;
	}

	// /**
	// * Get Oracle's oracle.sql.DATE object by birthday(support for oracle)
	// * @param age Age number
	// * @return
	// * @deprecated
	// */
	// public static DATE getOracleBirthDay(int age){
	// DATE oracleDate = new DATE(new
	// java.sql.Date(getBirthDay(age).getTime()));
	// return oracleDate;
	// }

	/**
	 * Change a java.util.Date to a TO_DATE() format string for oracle sql
	 * format
	 * 
	 * @param aDate
	 *            A java.util.Date Object
	 * @return
	 */
	public static String toOracleDateFormat(Object aDate) {
		StringBuffer buff = new StringBuffer();
		buff.append("TO_DATE('");
		if (aDate instanceof Date) {
			String dateStr = parseToDefaultDateString((Date) aDate);
			buff.append(dateStr).append("','YYYY-MM-DD HH24:MI:SS')");
			// don't include other package
			// }else if (aDate instanceof DATE){
			// DATE date= (DATE) aDate;
			// buff.append(date.stringValue()).append("','MM/DD/YYYY
			// HH24:MI:SS')");
		} else {
			return null;
		}
		return buff.toString();
	}

	/**
	 * Change a java.util.Date to a TO_DATE() format string for oracle sql
	 * format
	 * 
	 * @param aDate
	 *            A java.util.Date Object
	 * @return
	 */
	public static String toOracleTimeFormat(Object aDate) {
		StringBuffer buff = new StringBuffer();
		buff.append("TO_DATE('");
		if (aDate instanceof Date) {
			String dateStr = parseToDefaultTimeString((Date) aDate);
			buff.append(dateStr).append("','HH24:MI:SS')");
			// don't include other package
			// }else if (aDate instanceof DATE){
			// DATE date= (DATE) aDate;
			// buff.append(date.stringValue()).append("','MM/DD/YYYY
			// HH24:MI:SS')");
		} else {
			return null;
		}
		return buff.toString();
	}

	/**
	 * Change a java.util.Date to a TO_DATE() format string for oracle sql
	 * format
	 * 
	 * @param aDate
	 *            A java.util.Date Object
	 * @return
	 */
	public static String toOracleDateTimeFormat(Object aDate) {
		StringBuffer buff = new StringBuffer();
		buff.append("TO_DATE('");
		if (aDate instanceof Date) {
			String dateStr = parseToDefaultDateTimeString((Date) aDate);
			buff.append(dateStr).append("','YYYY-MM-DD HH24:MI:SS')");
			// don't include other package
			// }else if (aDate instanceof DATE){
			// DATE date= (DATE) aDate;
			// buff.append(date.stringValue()).append("','MM/DD/YYYY
			// HH24:MI:SS')");
		} else {
			return null;
		}
		return buff.toString();
	}

	/**
	 * Exchange a date format String into a TO_DATE format string for sql. The
	 * exchange principle is following an Oracle way
	 * 
	 * @param str
	 *            Date formate string , i.e. 2005-09-28 14:30
	 * @param patten
	 *            oracle date patten Parameter Explanation YEAR Year, spelled
	 *            out YYYY 4-digit year YYY YY Y Last 3, 2, or 1 digit(s) of
	 *            year. IYY IY I Last 3, 2, or 1 digit(s) of ISO year. IYYY
	 *            4-digit year based on the ISO standard RRRR Accepts a 2-digit
	 *            year and returns a 4-digit year. A value between 0-49 will
	 *            return a 20xx year. A value between 50-99 will return a 19xx
	 *            year. Q Quarter of year (1, 2, 3, 4; JAN-MAR = 1). MM Month
	 *            (01-12; JAN = 01). MON Abbreviated name of month. MONTH Name
	 *            of month, padded with blanks to length of 9 characters. RM
	 *            Roman numeral month (I-XII; JAN = I). WW Week of year (1-53)
	 *            where week 1 starts on the first day of the year and continues
	 *            to the seventh day of the year. W Week of month (1-5) where
	 *            week 1 starts on the first day of the month and ends on the
	 *            seventh. IW Week of year (1-52 or 1-53) based on the ISO
	 *            standard. D Day of week (1-7). DAY Name of day. DD Day of
	 *            month (1-31). DDD Day of year (1-366). DY Abbreviated name of
	 *            day. J Julian day; the number of days since January 1, 4712
	 *            BC. HH Hour of day (1-12). HH12 Hour of day (1-12). HH24 Hour
	 *            of day (0-23). MI Minute (0-59). SS Second (0-59). SSSSS
	 *            Seconds past midnight (0-86399). AM, A.M., PM, or P.M. AD or
	 *            A.D AD indicator BC or B.C. BC indicator TZD Daylight savings
	 *            information. For example, 'PST' TZH Time zone hour. TZM Time
	 *            zone minute. TZR Time zone region. Usage example：
	 *            toOracleDateFormat("2003/07/09","yyyy/mm/dd") returns
	 *            TO_DATE('2003/07/09', 'yyyy/mm/dd')
	 * @return
	 */
	public static String toOracleDateFormat(String str, String patten) {
		return "TO_DATE('" + str + "','" + patten + "')";
	}

	/**
	 * Parse a date format string into a java.util.Date object
	 * 
	 * @param dateStr
	 *            Date string
	 * @param dateFmt
	 *            Date format to parse the string
	 * @return
	 * @throws java.text.ParseException
	 *             If the string and the format is not match, exception will be
	 *             thrown
	 */
	public static Date toUtilDate(String dateStr, String dateFmt)
			throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat(dateFmt);
		return format.parse(dateStr);
	}

	/**
	 * Exchange day format string like yyyy-MM-dd HH:mm into a java.util.Date
	 * 
	 * @param dateStr
	 * @return
	 * @throws java.text.ParseException
	 */
	public static Date toDefaultDateTime(String dateStr) throws ParseException {
		Date date = null;
		try {
			date = toUtilDate(dateStr.trim(), DEFAULT_DATE_TIME);
		} catch (ParseException e) {
			date = toUtilDate(dateStr.trim(), DEFAULT_DATE);
		}
		return date;
	}

	/**
	 * Exchange day format string like yyyy-MM-dd HH:mm into a java.util.Date
	 * 
	 * @param dateStr
	 * @return
	 * @throws java.text.ParseException
	 */
	public static Date toDefaultDateTimeSecond(String dateStr)
			throws ParseException {
		Date date = null;
		try {
			date = toUtilDate(dateStr.trim(), DEFAULT_DATE_TIME_SECOND);
		} catch (ParseException e) {
			date = toUtilDate(dateStr.trim(), DEFAULT_DATE);
		}
		return date;
	}

	/**
	 * Exchange day format string like yyyy-MM-dd into a java.util.Date
	 * 
	 * @param date
	 * @return String
	 */
	public static String parseToDefaultDateSecondString(Date date) {
		return parseToFormatDateString(date, DEFAULT_DATE_TIME_SECOND);
	}

	/**
	 * Exchange day format string like yyyy-MM-dd into a java.util.Date
	 * 
	 * @param dateStr
	 * @return
	 * @throws java.text.ParseException
	 */
	public static Date toDefaultDate(String dateStr) throws ParseException {
		return toUtilDate(dateStr, DEFAULT_DATE);
	}

	/**
	 * Exchange day format string like HH:mm into a java.util.Date
	 * 
	 * @param dateStr
	 * @return
	 * @throws java.text.ParseException
	 */
	public static Date toDefaultTime(String dateStr) throws ParseException {
		return toUtilDate(dateStr, DEFAULT_TIME);
	}

	/**
	 * Merge the date and the time into one java.util.Date object
	 * 
	 * @param date
	 *            Date
	 * @param time
	 *            Time
	 * @return A java.util.Date object containing the date and the time
	 */
	public static Date mergeDateAndTime(Date date, Date time) {
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		// split the date into year, month and day
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		cal.setTime(time);
		cal.set(year, month, day);
		return cal.getTime();
	}

	/**
	 * Exchange the java.util.Date object into the DEFAULT_DATE_TIME formate
	 * string
	 * 
	 * @param date
	 * @return
	 */
	public static String parseToDefaultDateTimeString(Date date) {
		return parseToFormatDateString(date, DEFAULT_DATE_TIME);
	}

	/**
	 * Exchange the java.util.Date object into the DEFAULT_DATE formate string
	 * 
	 * @param date
	 * @return
	 */
	public static String parseToDefaultDateString(Date date) {
		return parseToFormatDateString(date, DEFAULT_DATE);
	}

	/**
	 * Exchange the java.util.Date object into the DEFAULT_TIME formate string
	 * 
	 * @param date
	 * @return
	 */
	public static String parseToDefaultTimeString(Date date) {
		return parseToFormatDateString(date, DEFAULT_TIME);
	}

	public static String parseToFormatDateString(Date date, String patten) {
		SimpleDateFormat format = new SimpleDateFormat(patten);
		if (date != null) {
			return format.format(date);
		} else {
			return "";
		}
	}

	/**
	 * Compare two java.util.Date object in date field(year, month and day)
	 * 
	 * @param date1
	 * @param date2
	 * @return If the two Date object are the same day, return true
	 */
	public static boolean isDateEqual(Date date1, Date date2) {
		if (date1 == null || date2 == null)
			// one of the input is null, skip the comparation
			return false;
		boolean rtnVal = true;
		Calendar cal1 = new GregorianCalendar();
		cal1.setTime(date1);
		Calendar cal2 = new GregorianCalendar();
		cal2.setTime(date2);
		// compare year
		rtnVal = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);
		// compare day
		if (rtnVal)
			// if two dates are in the same year, compare them with the day
			rtnVal = cal1.get(Calendar.DAY_OF_YEAR) == cal2
					.get(Calendar.DAY_OF_YEAR);
		return rtnVal;
	}

	/**
	 * Compare two date in time zone(hour,minute,no need to compare for second
	 * for it is useless)
	 * 
	 * @param date1
	 * @param date2
	 * @return true if the two date are the same in hour and minute
	 */
	public static final boolean isTimeEqual(Date date1, Date date2) {
		boolean rtnVal = true;
		Calendar cal1 = new GregorianCalendar();
		cal1.setTime(date1);
		Calendar cal2 = new GregorianCalendar();
		cal1.setTime(date2);
		rtnVal = cal1.get(Calendar.HOUR_OF_DAY) == cal2
				.get(Calendar.HOUR_OF_DAY);
		if (rtnVal)
			rtnVal = cal1.get(Calendar.MINUTE) == cal2.get(Calendar.MINUTE);
		return rtnVal;
	}

	/**
	 * Test whether a date is within an date range
	 * 
	 * @param date
	 *            The date to be test
	 * @param from
	 *            From range
	 * @param to
	 *            To range
	 * @return
	 */
	public static final boolean isBetween(Date date, Date from, Date to) {
		return from.before(date) && to.after(date);
	}

	/**
	 * Get the day of tomorrow.
	 * 
	 * @param date
	 *            Date
	 * @return Date
	 */
	public static Date getTomorrow(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, 1);

		return cal.getTime();
	}

	/**
	 * 获取中国农历年信息.
	 * 
	 * @param date
	 *            公历日期
	 * @return ChineseYear 农历年信息
	 */
	public static ChineseYear getChineseYear(Date date) {
		return new ChineseYear(date);
	}

	/**
	 * 返回生肖.
	 * 
	 * @param date
	 *            公历生日
	 * @return 生肖名
	 */
	public static String getAnimalName(Date date) {
		ChineseYear year = new ChineseYear(date);
		try {
			return year.getAnimalName();
		} finally {
			year = null; // release resources
		}
	}

	/**
	 * 中国农历类. <p/> 装载农历年月日和生肖等信息，如果是闰月，则显示月份会用-好显示，但是在toString()方法中
	 * 会转换为可直接输出的"闰"字
	 * </p>
	 * <p>
	 * Created by 2006-10-8
	 * </p>
	 * 
	 * @author <a href="mailto:jgnan@tom.com">jgnan</a>
	 */
	static class ChineseYear {
		private int chineseYear;

		private int chineseMonth;

		private int chineseDay;

		public ChineseYear(Date date) {
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTime(date);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH) + 1; // Calendar类的月份从0开始，运算月份从1开始，故要加1
			int day = cal.get(Calendar.DAY_OF_MONTH);
			if (year < 1901 || year > 2100) {
				throw new IllegalArgumentException(
						"Year must between from 1901 to 2100");
			}
			int startYear = 1901;
			int startMonth = 1;
			int startDay = 1;
			chineseYear = 4597;
			chineseMonth = 11;
			chineseDay = 11;
			// 第二个对应日，用以提高计算效率
			// 公历 2000 年 1 月 1 日，对应农历 4697 年 11 月 25 日
			if (year >= 2000) {
				startYear = 2000;
				startMonth = 1;
				startDay = 1;
				chineseYear = 4696;
				chineseMonth = 11;
				chineseDay = 25;
			}
			// 计算与基准日的天数差距
			int daysDiff = 0;
			for (int i = startYear; i < year; i++) {
				daysDiff += 365;
				if (isGregorianLeapYear(i))
					daysDiff += 1; // leap year
			}
			for (int i = startMonth; i < month; i++) {
				daysDiff += daysInGregorianMonth(year, i);
			}
			daysDiff += day - startDay;
			// 计算农历日
			chineseDay += daysDiff;
			int lastDay = daysInChineseMonth(chineseYear, chineseMonth);
			int nextMonth = nextChineseMonth(chineseYear, chineseMonth);
			while (chineseDay > lastDay) {
				if (Math.abs(nextMonth) < Math.abs(chineseMonth))
					chineseYear++;
				chineseMonth = nextMonth;
				chineseDay -= lastDay;
				lastDay = daysInChineseMonth(chineseYear, chineseMonth);
				nextMonth = nextChineseMonth(chineseYear, chineseMonth);
			}
		}

		public String getAnimalName() {
			return animalNames[(chineseYear - 1) % 12];
		}

        @Override
		public String toString() {
			StringBuffer buff = new StringBuffer();
			buff.append(stemNames[(chineseYear - 1) % 10]); // 天干纪年
			buff.append(branchNames[(chineseYear - 1) % 12]).append("年 "); // 地支纪年
			if (chineseMonth < 0) {
				buff.append("闰");
			}
			buff.append(monthNames[Math.abs(chineseMonth) - 1]).append("月");
			buff.append(dayNames[chineseDay - 1]);
			String show = buff.toString();
			buff = null;
			return show;
		}

		// 天干
		private final static String[] stemNames = { "甲", "乙", "丙", "丁", "戊",
				"己", "庚", "辛", "壬", "癸" };

		// 地支
		private final static String[] branchNames = { "子", "丑", "寅", "卯", "辰",
				"巳", "午", "未", "申", "酉", "戌", "亥" };

		// 月份中文名
		private final static String[] monthNames = { "正", "二", "三", "四", "五",
				"六", "七", "八", "九", "十", "冬", "腊" };

		private final static String[] dayNames = { "初一", "初二", "初三", "初四",
				"初五", "初六", "初七", "初八", "初九", "初十", "十一", "十二", "十三", "十四",
				"十五", "十六", "十七", "十八", "十九", "二十", "廿一", "廿二", "廿三", "廿四",
				"廿五", "廿六", "廿七", "廿八", "廿九", "三十", };

		// 生肖名
		private static String[] animalNames = { "鼠", "牛", "虎", "兔", "龙", "蛇",
				"马", "羊", "猴", "鸡", "狗", "猪" };

		// 大闰月的闰年年份
		private final static int[] bigLeapMonthYears = { 6, 14, 19, 25, 33, 36,
				38, 41, 44, 52, 55, 79, 117, 136, 147, 150, 155, 158, 185, 193 };

		private final static char[] daysInGregorianMonth = { 31, 28, 31, 30,
				31, 30, 31, 31, 30, 31, 30, 31 };

		private final static char[] chineseMonths = {
				// 农历月份大小压缩表，两个字节表示一年。两个字节共十六个二进制位数，
				// 前四个位数表示闰月月份，后十二个位数表示十二个农历月份的大小。
				0x00, 0x04, 0xad, 0x08, 0x5a, 0x01, 0xd5, 0x54, 0xb4, 0x09,
				0x64, 0x05, 0x59, 0x45, 0x95, 0x0a, 0xa6, 0x04, 0x55, 0x24,
				0xad, 0x08, 0x5a, 0x62, 0xda, 0x04, 0xb4, 0x05, 0xb4, 0x55,
				0x52, 0x0d, 0x94, 0x0a, 0x4a, 0x2a, 0x56, 0x02, 0x6d, 0x71,
				0x6d, 0x01, 0xda, 0x02, 0xd2, 0x52, 0xa9, 0x05, 0x49, 0x0d,
				0x2a, 0x45, 0x2b, 0x09, 0x56, 0x01, 0xb5, 0x20, 0x6d, 0x01,
				0x59, 0x69, 0xd4, 0x0a, 0xa8, 0x05, 0xa9, 0x56, 0xa5, 0x04,
				0x2b, 0x09, 0x9e, 0x38, 0xb6, 0x08, 0xec, 0x74, 0x6c, 0x05,
				0xd4, 0x0a, 0xe4, 0x6a, 0x52, 0x05, 0x95, 0x0a, 0x5a, 0x42,
				0x5b, 0x04, 0xb6, 0x04, 0xb4, 0x22, 0x6a, 0x05, 0x52, 0x75,
				0xc9, 0x0a, 0x52, 0x05, 0x35, 0x55, 0x4d, 0x0a, 0x5a, 0x02,
				0x5d, 0x31, 0xb5, 0x02, 0x6a, 0x8a, 0x68, 0x05, 0xa9, 0x0a,
				0x8a, 0x6a, 0x2a, 0x05, 0x2d, 0x09, 0xaa, 0x48, 0x5a, 0x01,
				0xb5, 0x09, 0xb0, 0x39, 0x64, 0x05, 0x25, 0x75, 0x95, 0x0a,
				0x96, 0x04, 0x4d, 0x54, 0xad, 0x04, 0xda, 0x04, 0xd4, 0x44,
				0xb4, 0x05, 0x54, 0x85, 0x52, 0x0d, 0x92, 0x0a, 0x56, 0x6a,
				0x56, 0x02, 0x6d, 0x02, 0x6a, 0x41, 0xda, 0x02, 0xb2, 0xa1,
				0xa9, 0x05, 0x49, 0x0d, 0x0a, 0x6d, 0x2a, 0x09, 0x56, 0x01,
				0xad, 0x50, 0x6d, 0x01, 0xd9, 0x02, 0xd1, 0x3a, 0xa8, 0x05,
				0x29, 0x85, 0xa5, 0x0c, 0x2a, 0x09, 0x96, 0x54, 0xb6, 0x08,
				0x6c, 0x09, 0x64, 0x45, 0xd4, 0x0a, 0xa4, 0x05, 0x51, 0x25,
				0x95, 0x0a, 0x2a, 0x72, 0x5b, 0x04, 0xb6, 0x04, 0xac, 0x52,
				0x6a, 0x05, 0xd2, 0x0a, 0xa2, 0x4a, 0x4a, 0x05, 0x55, 0x94,
				0x2d, 0x0a, 0x5a, 0x02, 0x75, 0x61, 0xb5, 0x02, 0x6a, 0x03,
				0x61, 0x45, 0xa9, 0x0a, 0x4a, 0x05, 0x25, 0x25, 0x2d, 0x09,
				0x9a, 0x68, 0xda, 0x08, 0xb4, 0x09, 0xa8, 0x59, 0x54, 0x03,
				0xa5, 0x0a, 0x91, 0x3a, 0x96, 0x04, 0xad, 0xb0, 0xad, 0x04,
				0xda, 0x04, 0xf4, 0x62, 0xb4, 0x05, 0x54, 0x0b, 0x44, 0x5d,
				0x52, 0x0a, 0x95, 0x04, 0x55, 0x22, 0x6d, 0x02, 0x5a, 0x71,
				0xda, 0x02, 0xaa, 0x05, 0xb2, 0x55, 0x49, 0x0b, 0x4a, 0x0a,
				0x2d, 0x39, 0x36, 0x01, 0x6d, 0x80, 0x6d, 0x01, 0xd9, 0x02,
				0xe9, 0x6a, 0xa8, 0x05, 0x29, 0x0b, 0x9a, 0x4c, 0xaa, 0x08,
				0xb6, 0x08, 0xb4, 0x38, 0x6c, 0x09, 0x54, 0x75, 0xd4, 0x0a,
				0xa4, 0x05, 0x45, 0x55, 0x95, 0x0a, 0x9a, 0x04, 0x55, 0x44,
				0xb5, 0x04, 0x6a, 0x82, 0x6a, 0x05, 0xd2, 0x0a, 0x92, 0x6a,
				0x4a, 0x05, 0x55, 0x0a, 0x2a, 0x4a, 0x5a, 0x02, 0xb5, 0x02,
				0xb2, 0x31, 0x69, 0x03, 0x31, 0x73, 0xa9, 0x0a, 0x4a, 0x05,
				0x2d, 0x55, 0x2d, 0x09, 0x5a, 0x01, 0xd5, 0x48, 0xb4, 0x09,
				0x68, 0x89, 0x54, 0x0b, 0xa4, 0x0a, 0xa5, 0x6a, 0x95, 0x04,
				0xad, 0x08, 0x6a, 0x44, 0xda, 0x04, 0x74, 0x05, 0xb0, 0x25,
				0x54, 0x03 };

		private boolean isGregorianLeapYear(int year) {
			boolean isLeap = false;
			if (year % 4 == 0)
				isLeap = true;
			if (year % 100 == 0)
				isLeap = false;
			if (year % 400 == 0)
				isLeap = true;
			return isLeap;
		}

		/*
		 * 计算农历日期
		 */
		private int daysInChineseMonth(int y, int m) {
			// 注意：闰月 m < 0
			int index = y - 4597;
			int v = 0;
			int l = 0;
			int d = 30;
			if (1 <= m && m <= 8) {
				v = chineseMonths[2 * index];
				l = m - 1;
				if (((v >> l) & 0x01) == 1)
					d = 29;
			} else if (9 <= m && m <= 12) {
				v = chineseMonths[2 * index + 1];
				l = m - 9;
				if (((v >> l) & 0x01) == 1)
					d = 29;
			} else {
				v = chineseMonths[2 * index + 1];
				v = (v >> 4) & 0x0F;
				if (v != Math.abs(m)) {
					d = 0;
				} else {
					d = 29;
					for (int i = 0; i < bigLeapMonthYears.length; i++) {
						if (bigLeapMonthYears[i] == index) {
							d = 30;
							break;
						}
					}
				}
			}
			return d;
		}

		/*
		 * 计算下一个农历月的日期
		 */
		private int nextChineseMonth(int y, int m) {
			int n = Math.abs(m) + 1;
			if (m > 0) {
				int index = y - 4597;
				int v = chineseMonths[2 * index + 1];
				v = (v >> 4) & 0x0F;
				if (v == m)
					n = -m;
			}
			if (n == 13)
				n = 1;
			return n;
		}

		/*
		 * 计算公历月份
		 */
		private int daysInGregorianMonth(int y, int m) {
			int d = daysInGregorianMonth[m - 1];
			if (m == 2 && isGregorianLeapYear(y))
				d++; // 公历闰年二月多一天
			return d;
		}
	}

	/**
	 * 判断 date是否大于等于开始日期,小于等于结束日期
	 * 
	 * @param date
	 *            指定某个日期
	 * @param from
	 *            开始日期
	 * @param to
	 *            结束日期
	 * @return
	 * @author: zhanghailang
	 */
	public static boolean isDateInRange(Date date, Date from, Date to) {
		if ((date.after(from) && date.before(to)) || date.compareTo(from) == 0
				|| date.compareTo(to) == 0) {
			return true;
		} else
			return false;
	}

	/**
	 * 
	 */
	public static String getLastLoginTimeString(Date lastSignonTime) {
		int day = daysBetween(new Date(), lastSignonTime);
		String lastLoginTime = "";
		if (day <= 1)
			lastLoginTime = "24小时内";
		if (day > 1 && day <= 3)
			lastLoginTime = "3天内";
		else if (day > 3 && day <= 7)
			lastLoginTime = "1个星期内";
		else if (day > 7 && day <= 15)
			lastLoginTime = "半个月内";
		else if (day > 15)
			lastLoginTime = "半月以上";

		return lastLoginTime;
	}
	
}
