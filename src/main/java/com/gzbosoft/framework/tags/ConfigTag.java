package com.gzbosoft.framework.tags;

import com.gzbosoft.framework.config.Configurator;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

/**
 * 获取配置文件
 */
public class ConfigTag extends SimpleTagSupport {
    public String key;

    /**
     * @return Returns the key.
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key The key to set.
     */
    public void setKey(String key) {
        this.key = key;
    }

    public ConfigTag() {
        super();
    }

    public void doTag() throws JspException, IOException {
        getJspContext().getOut().print(Configurator.getString(getKey()));
    }
}
