package com.gzbosoft.framework.web.action;

import org.apache.struts2.rest.DefaultHttpHeaders;

/**
 * Created by IntelliJ IDEA.
 * User: roymax
 * Date: 12-8-5
 * Time: 下午1:09
 * To change this template use File | Settings | File Templates.
 */
public abstract class CURDAction extends BaseAction {

    public abstract String index() throws Exception;

    public abstract String editNew() throws Exception;

    public abstract DefaultHttpHeaders create() throws Exception;

    public abstract String edit() throws Exception;

    public abstract String update() throws Exception;

    public abstract String show() throws Exception;

    public abstract String destroy() throws Exception;


}
