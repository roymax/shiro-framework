package com.gzbosoft.framework.web.action;

/**
 * Created by IntelliJ IDEA.
 * User: roymax
 * Date: 12-8-5
 * Time: 下午1:11
 * To change this template use File | Settings | File Templates.
 */
public interface GlobalResults {
    /**
     * 返回当前频道的首页
     */
    public static final String HOME = "home";

    /**
     * 返回登录页
     */
    public static final String LOGIN = "login";

    /**
     * 跳到全局错误页
     */
    public static final String ERROR_500 = "500";

    /**
     * 跳到信息提示页
     */
    public static final String MESSAGE = "message";


    public static final String UPDATE = "update";

    public static final String SHOW = "show";
    
    
    public static final String EDIT = "edit";

    public static final String CREATE = "create";

    public static final String INDEX = "index";

}
