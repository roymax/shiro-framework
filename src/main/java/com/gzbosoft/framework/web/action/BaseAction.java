package com.gzbosoft.framework.web.action;

import com.opensymphony.xwork2.ActionSupport;

/**
 * Created by IntelliJ IDEA.
 * User: roymax
 * Date: 12-8-5
 * Time: 下午1:09
 * To change this template use File | Settings | File Templates.
 */
public abstract class BaseAction extends ActionSupport implements GlobalResults {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
