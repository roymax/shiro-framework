package com.gzbosoft.framework.config;

import java.util.Properties;

import org.apache.commons.configuration.AbstractFileConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationConverter;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Configurator {
	/**
	 * Logger for this class
	 */
	private static final Logger log = LoggerFactory
			.getLogger(Configurator.class);

	private static final String CONFIG_XML_FILENAME = "framework.xml";

	private static Object o = new Object();

	private static Configuration config = null;

	static {
		Configurator.init();
	}

	private Configurator() {
	}

	private static Properties getProperties(String file, boolean autoReload) {
		synchronized (o) {
			if (config == null) {
				init(file, autoReload);
			}
		}
		return ConfigurationConverter.getProperties(config);
	}

	public static Properties getProperties(String file) {
		return getProperties(file, false);
	}

	public static Properties getProperties() {
		return getProperties(CONFIG_XML_FILENAME);
	}

	public static void init(String file) {
		init(file, false);
	}

	private static void init(String file, boolean autoReload) {
		load(file, autoReload);
	}

	public static void init() {
		init(null);
	}

	@SuppressWarnings("unused")
	private static void load() {
		load(null, false);
	}

	@SuppressWarnings("unused")
	private static void load(String file) {
		load(file, false);
	}

	@SuppressWarnings("unused")
	private static void load(boolean autoReload) {
		load(null, autoReload);
	}

	private static void load(String configLocation, boolean autoReload) {
		if (log.isDebugEnabled()) {
			log.debug("Getting Config");
			log.debug("config AutoReload : {} ", autoReload);
			log.debug("config Location : {} ",
					configLocation == null ? CONFIG_XML_FILENAME
							: configLocation);
		}

		try {

			if (StringUtils.isNotBlank(configLocation)) {

				if (configLocation.endsWith(".properties")) {
					config = new PropertiesConfiguration("framework.properties");
				} else {
					config = new XMLConfiguration(configLocation);
				}
			} else {
				configLocation = CONFIG_XML_FILENAME;
				config = new XMLConfiguration(configLocation);
			}

			if (autoReload) {
				((AbstractFileConfiguration) config)
						.setReloadingStrategy(new FileChangedReloadingStrategy());
			}

			log.info("init framework config file completed : {} ",
					configLocation);
		} catch (ConfigurationException e) {
			log.error("Error reading {} {} ", CONFIG_XML_FILENAME, e);
			log.error("Ensure the {} file is readable and in your classpath.",
					CONFIG_XML_FILENAME);
		}
	}

	/**
	 * ֵ
	 * 
	 * @param key
	 * @param defaultValue
	 * @return
	 */
	public static int getInt(String key, int defaultValue) {
		return config.getInt(key, defaultValue);
	}

	public static int getInt(String key) {
		return getInt(key, -1);
	}

	public static long getLong(String key, long defaultValue) {
		return config.getLong(key, defaultValue);
	}

	public static long getLong(String key) {
		return getLong(key, -1);
	}

	public static boolean getBoolean(String key, boolean defaultValue) {
		return config.getBoolean(key, defaultValue);
	}

	public static boolean getBoolean(String key) {
		return config.getBoolean(key);
	}

	public static String[] getStringArray(String key) {
		return config.getStringArray(key);
	}

	/**
	 * @param key
	 * @return
	 */
	public static String getString(String key) {
		return config.getString(key);
	}

	/**
	 * @param key
	 * @return
	 */
	public static String getString(String key, String defaultValue) {
		return config.getString(key, defaultValue);
	}

	/**
	 * @param groupMaxCreate
	 * @return
	 */
	public static Integer getInteger(String key) {
		return config.getInt(key);
	}

	public static void main(String[] args) throws ConfigurationException {
		System.out.println(Configurator.getString("paths.js"));
		
	}

}
