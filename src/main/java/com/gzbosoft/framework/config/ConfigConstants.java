/**
 * framework
 * com.yxqz.framework.config
 * ConfigConstants.java
 *
 */
package com.gzbosoft.framework.config;

public interface ConfigConstants {

	/**
	 * 网站域名
	 */
	public static final String SITES_DOMAIN = "sites.domain";

	public static final String PATHS_JS = "paths.js";

	public static final String PATHS_CSS = "paths.css";

	public static final String PATHS_IMAGES = "paths.images";

}
