package com.gzbosoft.framework.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;


@SuppressWarnings("unchecked")

public class HibernateEntityDao<T> extends HibernateGenericDao implements EntityDao<T> {

    protected Class<T> entityClass;//

    public HibernateEntityDao() {
        entityClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     */
    protected Class<T> getEntityClass() {
        return entityClass;
    }

    /**
     * @see HibernateGenericDao#getId(Class, Object)
     */
    public T get(Serializable id) {
        return get(getEntityClass(), id);
    }

    /**
     * @see HibernateGenericDao#getAll(Class)
     */
    public List<T> getAll() {
        return getAll(getEntityClass());
    }

    /**
     * @see HibernateGenericDao#getAll(Class, String, boolean)
     */
    public List<T> getAll(String orderBy, boolean isAsc) {
        return getAll(getEntityClass(), orderBy, isAsc);
    }

    /**
     * @see HibernateGenericDao#removeById(Class, java.io.Serializable)
     */
    public void removeById(Serializable id) {
        removeById(getEntityClass(), id);
    }

    /**
     * @see HibernateGenericDao#createCriteria(Class, Criterion[])
     */
    public Criteria createCriteria(Criterion... criterions) {
        return createCriteria(getEntityClass(), criterions);
    }

    public Criteria getCriteria(Class clazz) {
        return getSession().createCriteria(clazz);
    }

    /**
     * @see HibernateGenericDao#createCriteria(Class, String, boolean,
     *      Criterion[])
     */
    public Criteria createCriteria(String orderBy, boolean isAsc, Criterion... criterions) {
        return createCriteria(getEntityClass(), orderBy, isAsc, criterions);
    }

    /**
     */
    public List<T> findBy(String propertyName, Object value) {
        return findBy(getEntityClass(), propertyName, value);
    }

    /**
     */
    public List<T> findBy(String propertyName, Object value, String orderBy, boolean isAsc) {
        return findBy(getEntityClass(), propertyName, value, orderBy, isAsc);
    }

    public T findUniqueBy(String propertyName, Object value) {
        return findUniqueBy(getEntityClass(), propertyName, value);
    }

    public boolean isUnique(Object entity, String names) {
        return isUnique(getEntityClass(), entity, names);
    }




}
