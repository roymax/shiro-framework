package com.gzbosoft.framework.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class Paging implements Serializable {

    public static int DEFAULT_PAGE_SIZE = 20;

    private List data;

    private int pageSize = DEFAULT_PAGE_SIZE;

    private long start;

    private long totalCount;

    public Paging(int pageSize) {
        this(0, 0, pageSize, new ArrayList());
    }

    public Paging() {
        this(0, 0, DEFAULT_PAGE_SIZE, new ArrayList());
    }

    public Paging(long start, long totalSize, int pageSize, List data) {
        this.pageSize = pageSize;
        this.start = start;
        this.totalCount = totalSize;
        this.data = data;
    }

    public long getCurrentPageNo() {
        return start / pageSize + 1;
    }

    public int getPageSize() {
        return pageSize;
    }

    public List getResult() {
        return data;
    }

    public long getTotalCount() {
        return this.totalCount;
    }

    public long getTotalPageCount() {
        if (totalCount % pageSize == 0)
            return totalCount / pageSize;
        else
            return totalCount / pageSize + 1;
    }

    public boolean hasNextPage() {
        return this.getCurrentPageNo() < this.getTotalPageCount() - 1;
    }

    public boolean hasPreviousPage() {
        return this.getCurrentPageNo() > 1;
    }
}