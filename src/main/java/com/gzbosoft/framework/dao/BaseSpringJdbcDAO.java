/**
 * @author Nigel
 * Create Time: 2006-3-7
 */
package com.gzbosoft.framework.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class BaseSpringJdbcDAO  extends JdbcDaoSupport {
	/**
	 * 根据sql产生mysql的分页查询sql
	 * @param sql 查询语句
	 * @param begin 开始的取的行数 从1开始
	 * @param interval 取出的行数
	 * @return
	 */
	public String genPageSql(String sql, int begin, int interval) {
		sql =  sql + " limit " + begin+","+interval;
		return sql;
	}

	/**
	 * 分页查询
	 * @param sql 查询语句
	 * @param params
	 * @param begin 开始的取的行数 从1开始
	 * @param interval 取出的行数
	 * @return 
	 * @throws org.springframework.dao.DataAccessException
	 */
	public List queryForPager(String sql, Object[] params, int begin, int interval)
			throws DataAccessException {
		return getJdbcTemplate().queryForList(genPageSql(sql, begin, interval), params);
	}
}
