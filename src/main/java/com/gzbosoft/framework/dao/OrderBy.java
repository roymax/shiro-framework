package com.gzbosoft.framework.dao;

public class OrderBy {
	private String propertyName;
	private boolean ascending;
	private boolean ignoreCase;

	protected OrderBy(String propertyName, boolean ascending) {
		this.propertyName = propertyName;
		this.ascending = ascending;
		this.ignoreCase = false;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public boolean isAscending() {
		return ascending;
	}

	public boolean isIgnoreCase() {
		return ignoreCase;
	}

	public static OrderBy asc(String propertyName) {
		return new OrderBy(propertyName, true);
	}

	public static OrderBy desc(String propertyName) {
		return new OrderBy(propertyName, false);
	}

	public OrderBy ignoreCase() {
		this.ignoreCase = true;
		return this;
	}
}
