package com.gzbosoft.framework.dao;

import java.io.Serializable;
import java.util.List;

public interface EntityDao<T> {

	T get(Serializable id);

	List<T> getAll();

	void saveOrUpdate(Object o);

	void remove(Object o);

	void removeById(Serializable id);

	String getIdName(Class clazz);
	
	void update(Object o);
	
	Serializable save(Object o);
}
 