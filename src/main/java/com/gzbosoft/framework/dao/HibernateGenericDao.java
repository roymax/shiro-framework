package com.gzbosoft.framework.dao;

import com.gzbosoft.framework.utils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.*;
import org.hibernate.impl.CriteriaImpl;
import org.hibernate.metadata.ClassMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.util.Assert;
import org.springframework.util.ReflectionUtils;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 */
@SuppressWarnings("unchecked")


public class HibernateGenericDao extends HibernateDaoSupport {

    public final static String ASC = "asc";
    public final static String DESC = "desc";
    /**
     * Logger for this class
     */
    private static final Logger logger = LoggerFactory.getLogger(HibernateGenericDao.class);

    @Autowired
    public void setHibernateTemplate0(HibernateTemplate hibernateTemplate) {
        super.setHibernateTemplate(hibernateTemplate);
    }

    public <T> T get(Class<T> entityClass, Serializable id) {
        return (T) getHibernateTemplate().get(entityClass, id);
    }


    public <T> List<T> getAll(Class<T> entityClass) {
        return getHibernateTemplate().loadAll(entityClass);
    }

    public <T> List<T> getAll(Class<T> entityClass, String orderBy, boolean isAsc) {
        Assert.hasText(orderBy);
        if (isAsc)
            return getHibernateTemplate().findByCriteria(DetachedCriteria.forClass(entityClass).addOrder(Order.asc(orderBy)));
        else
            return getHibernateTemplate().findByCriteria(DetachedCriteria.forClass(entityClass).addOrder(Order.desc(orderBy)));
    }

    public void saveOrUpdate(Object o) {
        getHibernateTemplate().saveOrUpdate(o);
    }

    public Serializable save(Object o) {
        return getHibernateTemplate().save(o);
    }

    public void update(Object o) {
        getHibernateTemplate().update(o);
    }

    public <T> T merge(T object) {
        return (T) getHibernateTemplate().merge(object);
    }

    public void remove(Object o) {
        getHibernateTemplate().delete(o);
    }

    public <T> void removeById(Class<T> entityClass, Serializable id) {
        remove(get(entityClass, id));
    }

    public void flush() {
        getHibernateTemplate().flush();
    }

    public void clear() {
        getHibernateTemplate().clear();
    }

    private Query createQuery(String hql, Object... values) {
        Assert.hasText(hql);
        Query query = getSession().createQuery(hql);
        for (int i = 0; i < values.length; i++) {
            query.setParameter(i, values[i]);
        }
        return query;
    }

    public <T> Criteria createCriteria(Class<T> entityClass, Criterion... criterions) {
        Criteria criteria = getSession().createCriteria(entityClass);
        for (Criterion c : criterions) {
            criteria.add(c);
        }
        return criteria;
    }

    public <T> Criteria createCriteria(Class<T> entityClass, String orderBy, boolean isAsc, Criterion... criterions) {
        Assert.hasText(orderBy);

        Criteria criteria = createCriteria(entityClass, criterions);

        if (isAsc)
            criteria.addOrder(Order.asc(orderBy));
        else
            criteria.addOrder(Order.desc(orderBy));

        return criteria;
    }

    public List find(String hql, Object... values) {
        Assert.hasText(hql);
        return getHibernateTemplate().find(hql, values);
    }

    public Object findOne(String hql, Object... values) {
        Assert.hasText(hql);
        List result = find(hql, values);
        return result.size() == 0 ? null : result.get(0);
    }

    public <T> List<T> findBy(Class<T> entityClass, String propertyName, Object value) {
        Assert.hasText(propertyName);
        return createCriteria(entityClass, Restrictions.eq(propertyName, value)).list();
    }

    public <T> List<T> findBy(Class<T> entityClass, String propertyName, Object value, String orderBy, boolean isAsc) {
        Assert.hasText(propertyName);
        Assert.hasText(orderBy);
        return createCriteria(entityClass, orderBy, isAsc, Restrictions.eq(propertyName, value)).list();
    }

    public <T> T findUniqueBy(Class<T> entityClass, String propertyName, Object value) {
        Assert.hasText(propertyName);
        return (T) createCriteria(entityClass, Restrictions.eq(propertyName, value)).uniqueResult();
    }

    public Paging pagedQuery(String hql, int start, int range, Object... values) {
        Assert.hasText(hql);
        Assert.isTrue(start >= 0, "should start from 0");
        StringBuffer countBuffer = new StringBuffer(256);
        countBuffer.append(" select count (*) ").append(removeSelect(removeOrders(hql)));
        List countlist = getHibernateTemplate().find(countBuffer.toString(), values);
        long totalCount = (Long) countlist.get(0);

        if (totalCount < 1) {
            return new Paging();
        }
        Query query = createQuery(hql, values);
        List list = query.setFirstResult(start).setMaxResults(range).list();

        return new Paging(start, totalCount, range, list);
    }

    public Paging pagedQuery(Criteria criteria, int start, int range) {
        Assert.notNull(criteria);
        Assert.isTrue(start >= 0, "should start from 0");
        CriteriaImpl impl = (CriteriaImpl) criteria;

        Projection projection = impl.getProjection();
        List<CriteriaImpl.OrderEntry> orderEntries;
        try {
            orderEntries = (List) BeanUtils.forceGetProperty(impl, "orderEntries");
            BeanUtils.forceSetProperty(impl, "orderEntries", new ArrayList());
        } catch (Exception e) {
            throw new InternalError(" Runtime Exception impossibility throw ");
        }

        long totalCount = (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();

        criteria.setProjection(projection);
        if (projection == null) {
            criteria.setResultTransformer(CriteriaSpecification.ROOT_ENTITY);
        }

        try {
            BeanUtils.forceSetProperty(impl, "orderEntries", orderEntries);
        } catch (Exception e) {
            throw new InternalError(" Runtime Exception impossibility throw ");
        }

        if (totalCount < 1)
            return new Paging();

        List list = criteria.setFirstResult(start).setMaxResults(range).list();
        return new Paging(start, totalCount, range, list);
    }

    public Paging pagedQuery(Class entityClass, int start, int range, Criterion... criterions) {
        Criteria criteria = createCriteria(entityClass, criterions);
        return pagedQuery(criteria, start, range);
    }

    public Paging pagedQuery(Class entityClass, int start, int range, String orderBy, boolean isAsc, Criterion... criterions) {
        Criteria criteria = createCriteria(entityClass, orderBy, isAsc, criterions);
        return pagedQuery(criteria, start, range);
    }

    public <T> boolean isUnique(Class<T> entityClass, Object entity, String names) {
        Assert.hasText(names);
        Criteria criteria = createCriteria(entityClass).setProjection(Projections.rowCount());
        String[] nameList = names.split(",");
        try {
            for (String name : nameList) {
                criteria.add(Restrictions.eq(name, PropertyUtils.getProperty(entity, name)));
            }

            String idName = getIdName(entityClass);

            Serializable id = getId(entityClass, entity);

            if (id != null)
                criteria.add(Restrictions.not(Restrictions.eq(idName, id)));
        } catch (Exception e) {
            ReflectionUtils.handleReflectionException(e);
        }
        return (Integer) criteria.uniqueResult() == 0;
    }

    public Serializable getId(Class entityClass, Object entity) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Assert.notNull(entity);
        Assert.notNull(entityClass);
        return (Serializable) PropertyUtils.getProperty(entity, getIdName(entityClass));
    }

    public String getIdName(Class clazz) {
        Assert.notNull(clazz);
        ClassMetadata meta = getSessionFactory().getClassMetadata(clazz);
        Assert.notNull(meta, "Class " + clazz + " not define in hibernate session factory.");
        String idName = meta.getIdentifierPropertyName();
        Assert.hasText(idName, clazz.getSimpleName() + " has no identifier property define.");
        return idName;
    }

    private String removeSelect(String hql) {
        Assert.hasText(hql);
        int beginPos = hql.toLowerCase().indexOf("from");
        Assert.isTrue(beginPos != -1, " hql : " + hql + " must has a keyword 'from'");
        return hql.substring(beginPos);
    }

    private String removeOrders(String hql) {
        Assert.hasText(hql);
        Pattern p = Pattern.compile("order\\s*by[\\w|\\W|\\s|\\S]*", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(hql);
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(sb, "");
        }
        m.appendTail(sb);
        return sb.toString();
    }

    /**
     * 根据hql处理分页查询
     *
     * @param hql
     * @param values
     * @param begin
     * @param interval
     * @return
     * @throws org.springframework.dao.DataAccessException
     *
     * @author <a href="mailto:roy.wei@m-time.com">Roy Wei</a><br>
     * Create Time 2006-3-14 19:09:00
     */
    public List queryForPager(final String hql, final Map values, final int begin, final int interval) {
        Assert.hasText(hql, "查询语句为空！");
        return getHibernateTemplate().executeFind(new HibernateCallback() {

            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                Query query = session.createQuery(hql);

                if (values != null) {
                    Set set = values.keySet();
                    for (Iterator iter = set.iterator(); iter.hasNext(); ) {
                        String key = (String) iter.next();
                        query.setParameter(key, values.get(key));
                    }
                }
                query.setFirstResult(begin);
                query.setMaxResults(interval);
                if (logger.isDebugEnabled()) {
                    logger.debug(hql);
                }
                return query.list();
            }
        });
    }

    /**
     * 根据hql取记录总数
     *
     * @param hql
     * @param values
     * @return
     * @throws org.springframework.dao.DataAccessException
     *
     * @author <a href="mailto:roy.wei@m-time.com">Roy Wei</a><br>
     * Create Time 2006-3-14 20:23:59
     */
    public long getRecordCount(String hql, final Map values) {
        Assert.hasText(hql, "查询语句为空！");
        final String countQueryString = " select count (*) " + removeSelect(removeOrders(hql.replace("fetch", "")));

        Long total = (Long) getHibernateTemplate().execute(new HibernateCallback() {

            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                Query query = session.createQuery(countQueryString);

                if (values != null) {
                    Set set = values.keySet();
                    for (Iterator iter = set.iterator(); iter.hasNext(); ) {
                        String key = (String) iter.next();
                        query.setParameter(key, values.get(key));
                    }
                }
                return query.list().iterator().next();
            }
        });
        if (logger.isDebugEnabled()) {
            logger.debug(hql);
        }
        return (total == null ? 0 : total);
    }

    /**
     * 根据hql处理分页查询
     *
     * @param hql      hibernate 查询语句
     * @param values   参数数组
     * @param begin    起始行号
     * @param interval 每页记录数
     * @return
     * @author Nigel Create Time: 2006-3-8
     */
    public List queryForPager(String hql, Object[] values, int begin, int interval) {
        return queryForPager(hql, values, begin, interval, false);
    }

    /**
     * 根据hql处理分页查询
     *
     * @param hql        hibernate 查询语句
     * @param values     参数数组
     * @param begin      起始行号
     * @param interval   每页记录数
     * @param cachedable 是否使用缓存机制
     * @return
     */
    public List queryForPager(String hql, Object[] values, int begin, int interval, boolean cachedable) {
        Assert.hasText(hql, "查询语句为空！");
        List result = null;
        Query query = this.getSession(true).createQuery(hql);
        query.setCacheable(cachedable);
        if (values != null && values.length > 0) {
            for (int i = 0; i < values.length; i++) {
                Object object = values[i];
                query.setParameter(i, object);
            }
        }
        query.setFirstResult(begin);
        query.setMaxResults(interval);
        result = query.list();
        return result;
    }
//
//    /**
//     * 根据hql取记录总数
//     *
//     * @param hql
//     * @param values
//     * @return
//     * @author Nigel Create Time: 2006-3-8
//     */
//    public long getRecordCount(String hql, Object[] values) {
//        Assert.hasText(hql, "查询语句为空！");
//        long total = 0;
//
//        int orderIndex = hql.indexOf(" order ");
//        if (orderIndex > 0)// 把order by 子句去掉
//            hql = hql.substring(0, orderIndex);
//
//        if (hql.trim().startsWith("from"))
//            hql = "select count(*) " + hql;
//        else
//            hql = "select count(*) " + hql.substring(hql.indexOf(" from "));
//
//        Query query = this.getSession(true).createQuery(hql);
//        if (values != null && values.length > 0) {
//            for (int i = 0; i < values.length; i++) {
//                Object object = values[i];
//                query.setParameter(i, object);
//            }
//        }
//        Long temp = (Long) query.list().iterator().next();
//        total = (temp == null ? 0 : temp.longValue());
//
//        return total;
//    }

    public long getTotalCount(final String hql, final Map values) {
        Assert.hasText(hql, "查询语句为空！");

        Long total = (Long) getHibernateTemplate().execute(new HibernateCallback() {

            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                Query query = session.createQuery(hql);

                if (values != null) {
                    Set set = values.keySet();
                    for (Iterator iter = set.iterator(); iter.hasNext(); ) {
                        String key = (String) iter.next();
                        query.setParameter(key, values.get(key));
                    }
                }
                return query.list().iterator().next();
            }
        });
        if (logger.isDebugEnabled()) {
            logger.debug(hql);
        }
        return (total == null ? 0 : total);
    }

    /**
     * 根据语句进行批量操作
     *
     * @param hql
     * @param values
     * @return
     * @throws org.springframework.dao.DataAccessException
     *
     * @author Nigel Create Time: 2006-3-21
     */
    public int executeUpdate(String hql, Object[] values) {
        Assert.hasText(hql, "查询语句为空！");
        Query query = getSession().createQuery(hql);
        if (values != null && values.length > 0) {
            for (int i = 0; i < values.length; i++) {
                Object object = values[i];
                query.setParameter(i, object);
            }
        }
        return query.executeUpdate();
    }


    public void evict(Object entityClass) {
        getHibernateTemplate().evict(entityClass);
    }


    /**
     * 返回查询数据记录数
     *
     * @return 记录数
     */
    protected int count(Criteria criteria) {
        int total = (Integer) criteria.setProjection(Projections.count("id")).uniqueResult();
        criteria.setProjection(null);
        criteria.setResultTransformer(CriteriaSpecification.ROOT_ENTITY);
        return total;
    }

    private <T> void addExpression(Class<T> modelClass, Criteria criteria, String propertyName, String propertyValue) {
        Object value;
        try {
            Object bean = modelClass.newInstance();
            Class returnType = null;
            if (propertyName.endsWith("_SDate") || propertyName.endsWith("_EDate")) {
                String pn = propertyName.substring(0, propertyName.length() - 6);
                returnType = PropertyUtils.getPropertyType(bean, pn);
            } else {
                returnType = PropertyUtils.getPropertyType(bean, propertyName);
            }

            if (returnType == String.class) {
                BeanUtils.setProperty(bean, propertyName, propertyValue);
                value = PropertyUtils.getProperty(bean, propertyName);
                criteria.add(Expression.like(propertyName, value + "%"));
            } else if (returnType == Date.class) {

                if (propertyName.endsWith("_SDate")) {
                    // 大于等于这个日期
                    Object[] date = handyDateExpression(criteria, propertyName, propertyValue, handleDates.length - 1);
                    if (date[0] != null)
                        criteria.add(Expression.ge(propertyName.substring(0, propertyName.length() - 6), date[0]));
                } else if (propertyName.endsWith("_EDate")) {
                    // 小于等于这个日期
                    Object[] date = handyDateExpression(criteria, propertyName, propertyValue, handleDates.length - 1);
                    if (date[0] != null) {
                        Calendar cal = Calendar.getInstance();
                        cal.setTime((Date) date[0]);
                        cal.add((Integer) date[1], 1);
                        criteria.add(Expression.le(propertyName.substring(0, propertyName.length() - 6), cal.getTime()));
                    }
                } else {
                    handleDateLimit(criteria, propertyName, propertyValue);
                }
            } else {
                if (propertyValue.contains(",")) {
                    List values = new LinkedList();
                    if (returnType == Integer.class) {
                        String[] strings = propertyValue.split(",");
                        for (String string : strings) {
                            if (NumberUtils.isNumber(string))
                                values.add(new Integer(string));
                        }
                    }
                    criteria.add(Expression.in(propertyName, values));
                } else {
                    BeanUtils.setProperty(bean, propertyName, propertyValue);
                    value = PropertyUtils.getProperty(bean, propertyName);
                    criteria.add(Expression.eq(propertyName, value));
                }
            }
            bean = null;
        } catch (Exception e) {
            e.printStackTrace();
        }

        // if ("id".equalsIgnoreCase(propertyName)) {
        // criteria.add(Expression.eq(propertyName,
        // Integer.valueOf(propertyValue)));
        // } else {
        // criteria.add(Expression.like(propertyName, propertyValue + "%"));
        // }
    }

    /**
     * 对日期做处理 <p/>
     * <p/>
     * <pre>
     *   增加日期查询宽度,
     *   	 例如
     *   	 2006-05-05 查5号的资料
     *   	 2006-05 查5月的资料
     *   	 2006 查2006年的资料
     * </pre> *
     *
     * @param criteria
     * @param propertyName
     * @param propertyValue
     */
    public void handleDateLimit(Criteria criteria, String propertyName, String propertyValue) {
        // handleMinuteExpression(criteria, propertyName, propertyValue);
        Object[] date = handyDateExpression(criteria, propertyName, propertyValue, handleDates.length - 1);
        if (date[0] != null) {
            addDateExpression(criteria, propertyName, propertyValue, (Integer) date[1], (Date) date[0]);
        }
    }

    protected Object[] handyDateExpression(Criteria criteria, String propertyName, String propertyValue, int start) {
        int calendarType = 0;
        Date date = null;
        try {
            // special handle for java.util.Date property
            String dateFormat = (String) handleDates[start][1];

            calendarType = (Integer) handleDates[start][0];

            SimpleDateFormat formate = new SimpleDateFormat(dateFormat);
            date = formate.parse(propertyValue);
        } catch (Exception e) {
            logger.debug("handyDateExpression(Criteria, String, String, " + start + ")", e);
            if (start > 0)
                return handyDateExpression(criteria, propertyName, propertyValue, --start);
        }
        return new Object[]{date, calendarType};
    }

    private void addDateExpression(Criteria criteria, String propertyName, String propertyValue, int calendarType, Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(calendarType, 1);
        // criteria.add(Expression.le(propertyName, cal.getTime()));
        criteria.add(Expression.between(propertyName, date, cal.getTime()));
    }

    private static Object[][] handleDates = {{Calendar.YEAR, "yyyy"}, {Calendar.MONTH, "yyyy-MM"}, {Calendar.DATE, "yyyy-MM-dd"},
            {Calendar.HOUR_OF_DAY, "yyyy-MM-dd HH"}, {Calendar.MINUTE, "yyyy-MM-dd HH:mm"},};

}