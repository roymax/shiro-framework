package com.gzbosoft.security.dao;

import com.gzbosoft.framework.dao.HibernateEntityDao;
import com.gzbosoft.security.domain.RoleAuth;
import org.springframework.stereotype.Repository;

@Repository(value="roleAuthDao")
public class RoleAuthDao extends HibernateEntityDao<RoleAuth> {
}
