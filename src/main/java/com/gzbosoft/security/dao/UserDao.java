package com.gzbosoft.security.dao;

import com.gzbosoft.framework.dao.HibernateEntityDao;
import com.gzbosoft.framework.dao.Paging;
import com.gzbosoft.security.domain.Group;
import com.gzbosoft.security.domain.User;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: roymax
 * Date: 12-8-5
 * Time: 下午4:37
 * To change this template use File | Settings | File Templates.
 */

@Repository("userDao")
public class UserDao extends HibernateEntityDao<User> {

    /**
     * @param @param  groupId
     * @param @return
     * @return List<User>
     * @throws
     * @Title: getGroupAll
     * @Description: 获取某组下的所有用户
     */
    public List<User> getGroupAll(Integer groupId) {
        List<User> result = null;
        StringBuffer sb = new StringBuffer("from Group where id=" + groupId + "or parentId=" + groupId);
        List<Group> groups = find(sb.toString());

        Set<User> User = new HashSet<User>();

        if (null != groups && groups.size() > 0) {
            result = new ArrayList<User>();
            for (int i = 0; i < groups.size(); i++) {
                Group group = groups.get(i);
                Set<User> set = group.getUsers();
                Iterator<User> it = set.iterator();
                while (it.hasNext()) {
                    User.add(it.next());
                }
            }
        }

        Iterator<User> it = User.iterator();
        while (it.hasNext()) {
            result.add(it.next());
        }

        return result;
    }

    /**
     * @param @param  user 一条具体的用户信息
     * @param @param  page 当前页
     * @param @param  rows 一页显示多少条记录
     * @param @return
     * @return Pagination
     * @throws
     * @Title: getPageList
     * @Description: 具体某页显示多少条记录
     */
    public Paging getPageList(User user, int page, int rows, String sort, String order) {
        StringBuffer sb = new StringBuffer("from User where 1=1");
        if (null != user) {
            if (null != user.getUsername() && !"".equals(user.getUsername())) {
                sb.append(" and username like'%" + user.getUsername() + "%'");
            }
            if (null != user.getSex() && !"".equals(user.getSex())) {
                sb.append(" and sex like'%" + user.getSex() + "%'");
            }
            if (null != user.getBirthday() && !"".equals(user.getBirthday())) {
                sb.append(" and birthday like'%" + user.getBirthday() + "%'");
            }
        }
        sb.append(" order by createdAt desc");

        if (sort != null && !"".equals(sort)) {
            sb.append(" " + sort);
            if (order != null && !"".equals(order)) {
                sb.append(" " + order);
            } else {
                sb.append(" desc");
            }
        }
        return pagedQuery(sb.toString(), page, rows);
    }

    /**
     * @param @param  groupId 组id
     * @param @param  user 具体用户信息
     * @param @return
     * @return Boolean
     * @throws
     * @Title: saveToGroup
     * @Description: 往组里面添加用户
     */
    public Boolean saveToGroup(String groupId, User user) {
        Boolean flag = false;
        try {
            Group group = (Group) this.getId(Group.class, Integer.valueOf(groupId));

            Set<User> set = group.getUsers();
            this.save(user);
            set.add(user);

            flag = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    /**
     * @param userName
     * @param password
     * @return User 验证成功返回User对象，失败返回null
     * @throws
     * @Title: login
     * @Description: 验证用户名和密码
     * @author WangMing wang1988ming@qq.com
     */
    public User getByUsernameAndPassword(String userName, String password) {
        return (User)findOne("from User where username = ? and password= ?" , userName , password);
    }
//
//    /**
//     * @param @param  userId
//     * @param @return
//     * @return String
//     * @throws
//     * @Title: returnGroupName
//     * @Description: TODO(这里用一句话描述这个方法的作用)
//     */
//    public String returnRoleName(String userId) {
//        User user = (User) this.getHandler().findObj("from User where logonid ='" + userId + "'");
//        Set<Group> sets = user.getGroup();
//        StringBuffer sb = new StringBuffer("");
//        /*
//               * 这是按一个用户只能有一个组,来设计的
//               */
//        Iterator<Group> it = sets.iterator();
//        while (it.hasNext()) {
//            Group group = it.next();
//
//            Set<Role> roles = group.getRoles();
//            Iterator<Role> temp = roles.iterator();
//            while (temp.hasNext()) {
//                sb.append("," + temp.next().getEnname());
//            }
//        }
//
//        return sb.toString();
//    }
//
//    /**
//     * @param @param  user
//     * @param @param  groupId
//     * @param @return
//     * @return boolean
//     * @throws
//     * @Title: alterToGroup
//     * @Description:
//     */
//    public boolean alterToGroup(User user, String groupId) {
//        Set<Group> set = new HashSet<Group>();
//        set.add((Group) this.getHandler().findObj("from Group where id='" + groupId + "'"));
//        if (this.getHandler().alterObj(user)) {
//            user.setGroup(set);
//            return true;
//        }
//
//        return false;
//    }
//
//    public List<User> findByLoginIds(String ids) {
//        String[] strs = ids.split(",");
//        List<User> list = new ArrayList<User>();
//        for (int i = 0; i < strs.length; i++) {
//            User user = new User();
//            user = (User) this.getHandler().findObj("from User where logonid ='" + strs[i] + "'");
//            list.add(user);
//        }
//        return list;
//    }

}
