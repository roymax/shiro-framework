/**   
  * @Title: RoleDao.java 
  * @Package com.tgyt.permissions.dao 
  * @Description: 
  * @author WangMing wang1988ming@qq.com 
  * @date 2011-9-23 下午2:04:26 
  * @version V1.0   
  */

package com.gzbosoft.security.dao;

import com.gzbosoft.framework.dao.HibernateEntityDao;
import com.gzbosoft.framework.dao.Paging;
import com.gzbosoft.security.domain.Role;
import org.springframework.stereotype.Repository;


@Repository(value="roleDao")
public class RoleDao extends HibernateEntityDao<Role> {
	/** 
	  * @Title: getPageList 
	  * @Description: 根据当前页数与行数得到分页信息
	  * @param  role
	  * @param  pageNo
	  * @param  pageSize
	  * @return Pagination
	  * @throws 
	  */
	public Paging getPageList(Role role,int pageNo,int pageSize,String sort,String order){
		StringBuffer hql = new StringBuffer("from Role where 1=1 ");
		if(role!=null){
			String name = role.getName();
			String code = role.getCode();
			String status = role.getStatus();
			Integer orderid = role.getOrderBy();
			String memo = role.getMemo();
			if(!name.equals("")){
				hql.append(" and name like '%"+name+"%'");
			}
			if(!code.equals("")){
				hql.append(" and code like '%"+code+"%'");
			}
			if(status!=null && !"".equals(status)){
				hql.append(" and status='"+status+"'");
			}
			if(orderid != null){
				hql.append(" and orderBy="+orderid);
			}
			if(!memo.equals("")){
				hql.append(" and memo like '%"+memo+"%'");
			}
		}
		if(sort!=null && !"".equals(sort)){
			hql.append(" order by "+sort);
			if(order!=null && !"".equals(order)){
				hql.append(" "+order);
			}else{
				hql.append(" desc");
			}
		}
		return  pagedQuery(hql.toString(),pageNo,pageSize);
	}
	/** 
	  * @Title: addRole 
	  * @Description: 增加一条角色信息到数据库 
	  * @param  role
	  * @return boolean 增加成功返回 true 失败返回false
	  * @throws 
	  */
	/*public boolean addRole(Role role){
		return this.getHandler().saveObj(role);
	}*/
	/** 
	  * @Title: updateRole 
	  * @Description: 根据角色Id更新数据库里面角色信息 
	  * @param  role
	  * @return boolean  更新成功返回 true 失败返回false
	  * @throws 
	  */
/*	public boolean updateRole(Role role){
		return this.getHandler().alterObj(role);
	}*/
	/** 
	  * @Title: deleteRole 
	  * @Description: 根据角色Id删除数据库里面角色信息 
	  * @param  role
	  * @return boolean
	  * @throws 
	  */
	/*public boolean deleteRole(Role role){
		return this.getHandler().deleteObj(role);
	}*/

}
