package com.gzbosoft.security.dao;

import com.gzbosoft.framework.dao.HibernateEntityDao;
import com.gzbosoft.security.domain.Action;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository(value = "actionDAO")
public class ActionDAO extends HibernateEntityDao<Action> {

    /**
     * @param @return
     * @return int
     * @throws
     * @Title: getMaxOrder
     * @Description:获取操作信息表中的排序最大值
     */
    public int getMaxOrder() {
        Object result = find("select max(order_by) from Action");
        if (result != null) {
            return (Integer) result;
        } else {
            return 0;
        }
    }

}
