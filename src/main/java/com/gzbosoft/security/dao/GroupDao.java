package com.gzbosoft.security.dao;

import com.gzbosoft.framework.dao.HibernateEntityDao;
import com.gzbosoft.security.domain.Group;
import com.gzbosoft.security.domain.Role;
import org.springframework.stereotype.Repository;

import java.util.*;


@Repository
public class GroupDao extends HibernateEntityDao<Group> {

    public List<Map<String, Object>> getTree(String id) {
        List<Map<String, Object>> nodes = new ArrayList<Map<String, Object>>();
        //存放每层资源的临时变量
        List<Map<String, Object>> temp = new ArrayList<Map<String, Object>>();
        //第一级资源
        List<Group> types = find("from Group where parentId is null");

        if (null == types || types.size() == 0) {
            return nodes;
        }
        for (Group type : types) {
            Map<String, Object> node = new HashMap<String, Object>();
            node.put("id", type.getId() + "");
            node.put("text", type.getName());
            nodes.add(node);
            temp.add(node);
        }
        //临时
        List<Map<String, Object>> doing = new ArrayList<Map<String, Object>>();
        doing.addAll(nodes);

        while (!doing.isEmpty()) {
            temp = new ArrayList<Map<String, Object>>();
            for (Map<String, Object> item : doing) {
                List<Object> children = new ArrayList<Object>();
                String pid = (String) item.get("id");
                List<Group> tempTypes = find("from Group where parentId = ? ", id);
                for (Group type : tempTypes) {
                    Map<String, Object> node = new HashMap<String, Object>();
                    node.put("id", type.getId() + "");
                    node.put("text", type.getName());
                    temp.add(node);
                    children.add(node);
                }
                item.put("children", children.toArray(new Object[children.size()]));
            }
            doing = temp;
        }
        return nodes;
    }

    /**
     * @param @param id
     * @param @param roleIds
     * @return void
     * @throws
     * @Title: removeGroupHaveRole
     * @Description: 移除该组的某些角色
     */
    public void removeGroupHaveRole(String id, String roleIds) {
        //TODO move to service
        Group group = (Group) get(Integer.valueOf(id));
        Set<Role> roles = group.getRoles();

//		String[] tem = roleIds.split(",");
//		for(int i=0;i<tem.length;i++){
//			Role role = (Role) this.getHandler().findObjById(Role.class, Integer.valueOf(tem[i].trim()));
//
//			roles.remove(role);
//		}

        group.setRoles(roles);
    }

    /**
     * @param @return
     * @return List<Role>
     * @throws
     * @Title: groupHaveRole
     * @Description: 查看该组所拥有的角色
     */
    public List<Role> groupHaveRole(String groupid) {
        List<Role> result = new ArrayList<Role>();
        Group group = (Group) get(Integer.valueOf(groupid));
        Set<Role> roles = group.getRoles();
        Iterator<Role> it = roles.iterator();
        while (it.hasNext()) {
            result.add(it.next());
        }

        return result;
    }

    /**
     * @param @param roleIds 角色id的组和
     * @param @param groupId 组id
     * @return void
     * @throws
     * @Title: addRoleToGroup
     * @Description: 往组里添加角色
     */
    public void addRoleToGroup(List<Role> roles, Integer groupId) {
        Group group = (Group) get(groupId);
        Set<Role> groupRoles = group.getRoles();

//		String[] ids = roleIds.split(",");
//		for(int i=0;i<ids.length;i++){
//			Role temp = (Role)this.getHandler().findObjById(Role.class, Integer.valueOf(ids[i].trim()));
//			groupRoles.add(temp);
//		}
//
        group.setRoles(groupRoles);
    }


    /**
     * @param @param  nameGroup
     * @param @return
     * @return List<Group>
     * @throws
     * @Title: getGroup
     * @Description: 根据组名找组
     */
    public List<Group> getGroup(String nameGroup) {
        StringBuffer sb = new StringBuffer("from Group where 1=1");
        if (null != nameGroup && !"".equals(nameGroup)) {
            sb.append(" and name like '%" + nameGroup + "%'");
        }
        return find(sb.toString());
    }

    /**
     * @param @param userIds 用户id组和
     * @param @param groupId 组id
     * @return void
     * @throws
     * @Title: addUserToGroup
     * @Description: 往组里面添加用户
     */
//    public void addUserToGroup(String userIds, String groupId) {
//
//        String[] ids = userIds.split(",");
//        Group group = (Group) this.getHandler().findObjById(Group.class, Integer.valueOf(groupId));
//
//        Set<Users> set = group.getUsers();
//        for (int i = 0; i < ids.length; i++) {
//            set.add((Users) this.getHandler().findObjById(Users.class, Integer.valueOf(ids[i].trim())));
//        }
//
//        group.setUsers(set);
//    }

    /**
     * @param @param groupId 组id
     * @param @param id 用户id (多个用户的id例如：“2,3,”)
     * @return void
     * @throws
     * @Title: delUserFromGroup
     * @Description: 从组里面删除用户
     */
//    public void delUserFromGroup(String groupId, String id) {
//        Group group = (Group) this.getHandler().findObjById(Group.class, Integer.valueOf(groupId));
//        //没有删除之前，该组所有拥有的所有用户
//        Set<Users> all = group.getUsers();
//
//        id = id.substring(0, id.lastIndexOf(","));
//
//        String[] ids = id.split(",");
//        //要删除组内的多个用户
//        Set<Users> result = new HashSet<Users>();
//        for (int i = 0; i < ids.length; i++) {
//            Users user = (Users) this.getHandler().findObjById(Users.class, Integer.valueOf(ids[i]));
//            result.add(user);
//        }
//
//        if (all.removeAll(result)) {
//            group.setUsers(all);
//        }
//    }

    /**
     * @param @param groupId 组id
     * @return List<Role>
     * @throws
     * @Title: otherRoleFromGroup
     * @Description: 我要获取该组所没有的角色
     */
//    public List<Role> otherRoleFromGroup(String groupId) {
//        Group group = (Group) this.getHandler().findObjById(Group.class, Integer.valueOf(groupId));
//        //该组所拥有的角色
//        Set<Role> role = group.getRoles();
//
//        List<Role> groupList = new ArrayList<Role>();
//        Iterator<Role> it = role.iterator();
//        while (it.hasNext()) {
//            groupList.add(it.next());
//        }
//        //获取所有的角色 可以使用的角色
//        List<Role> list = this.getHandler().findListOfObj("from Role where status='status100'");
//
//        list.removeAll(groupList);
//
//        return list;
//    }

}
