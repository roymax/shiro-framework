package com.gzbosoft.security.dao;

import com.gzbosoft.framework.dao.HibernateEntityDao;
import com.gzbosoft.framework.dao.Paging;
import com.gzbosoft.security.domain.System;
import org.springframework.stereotype.Repository;

@Repository
public class SystemDao extends HibernateEntityDao<System> {

    /**
     * @param @param  system 一条具体Systems对象的信息
     * @param @param  page 当前页
     * @param @param  rows 一页显示多少记录
     * @param @return
     * @return Pagination
     * @throws
     * @Description: 获取具体某页的信息
     */
    public Paging pagedSystemByOrderBy(System system, int page, int rows, String sort, String order) {
        StringBuffer sb = new StringBuffer("from System where 1=1");
        if (null != system) {
            if (null != system.getName() && !"".equals(system.getName())) {
                sb.append(" and name like'%" + system.getName() + "%'");
            }
            if (null != system.getCode() && !"".equals(system.getCode())) {
                sb.append(" and code like'%" + system.getCode() + "%'");
            }
        }
        if (sort != null && !"".equals(sort)) {
            sb.append(" order by " + sort);
            if (order != null && !"".equals(order)) {
                sb.append(" " + order);
            } else {
                sb.append(" desc");
            }
        }
        return pagedQuery(sb.toString(), page, rows);
    }

}
