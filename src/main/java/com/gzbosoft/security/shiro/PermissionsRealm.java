package com.gzbosoft.security.shiro;

import com.gzbosoft.security.domain.*;
import com.gzbosoft.security.services.SecurityServices;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;


/**
 * @Description: 实际执行认证的类
 */
public class PermissionsRealm extends AuthorizingRealm {

    private static final Logger log = LoggerFactory.getLogger(PermissionsRealm.class);
//    User user;
//    Set<Role> userRoles;
//    Set<Resource> resources;
    @Autowired
    private SecurityServices securityService;

    public PermissionsRealm() {
        setName("PermissionsRealm");
    }

    /* (non-Javadoc)
      * <p>Title: doGetAuthorizationInfo</p>
      * <p>Description: 授权</p>
      * @param principals
      * @return
      * @see org.apache.shiro.realm.AuthorizingRealm#doGetAuthorizationInfo(org.apache.shiro.subject.PrincipalCollection)
      */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {

        String username = (String) principals.fromRealm(getName()).iterator().next();

        if (username != null) {
            log.debug("find user[{}] permissions..." , username);
            //查找用户所属角色
//            securityService.findUserByPaged();
            SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();

            info.addRole("admin");
            info.addStringPermission("user:create");
            return info;
        }
        
//        Set<Group> userGroups = user.getGroups();
//        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
//        if (user != null) {
//            for (Group group : userGroups) {
//                userRoles = group.getRoles();
//                for (Role role : userRoles) {
//                    info.addRole(role.getName());
//                    resources = role.getResources();
//                    for (Resource resource : resources) {
//                        RoleAuth roleAuth = securityService.getByRoleIdAndResourceId(role.getId(), resource.getId());
//                        if (roleAuth != null && roleAuth.getActions() != null && !"".equals(roleAuth.getActions())) {
//                            String[] actionString = roleAuth.getActions().split(",");
//                            for (String action : actionString) {
//                                //shiro权限字符串为：“当前资源英文名称:操作名英文名称”
//                                info.addStringPermission(action);
//                            }
//                        }
//                    }
//                }
//            }
//            return info;
//        }
        return null;
    }


    /* (non-Javadoc)
      * <p>Title: doGetAuthenticationInfo</p>
      * <p>Description: 认证</p>
      * @param authtoken
      * @return
      * @throws AuthenticationException
      * @see org.apache.shiro.realm.AuthenticatingRealm#doGetAuthenticationInfo(org.apache.shiro.authc.AuthenticationToken)
      */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(
            AuthenticationToken authtoken) throws AuthenticationException {
//        List<Resource> resourceList = new ArrayList<Resource>();
        UsernamePasswordToken token = (UsernamePasswordToken) authtoken;
//        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        String userName = token.getUsername();

        if (userName != null && !"".equals(userName)) {
            log.debug("user {} auth ..." , userName);
            User user = securityService.login(token.getUsername(), String.valueOf(token.getPassword()));

            if (user != null) {
                log.debug("user {} authenticated.", userName);
//                //获取所有的资源权限
//
//                Set<Group> userGroups = user.getGroups();
//                //SecurityUtils.getSubject().getSession().setAttribute("userGroup", userGroups);
//                for (Group group : userGroups) {
//                    log.debug("组名: {} \tid: {}" , group.getName() , group.getId());
//                    userRoles = group.getRoles();
//                    //SecurityUtils.getSubject().getSession().setAttribute("userRole", userRoles);
//                    for (Role role : userRoles) {
//                        log.debug("角色名: {} \tid: {}" , role.getName() , role.getId());
//                        info.addRole(role.getName());
//                        resources = role.getResources();
//                        //SecurityUtils.getSubject().getSession().setAttribute("userResource", resources);
//                        for (Resource resource : resources) {
//                            log.debug("资源: {} \tid: {}" , resource.getName() , resource.getId());
//                            resourceList.add(resource);
//                            //RoleAuth roleAuth = this.roleAuthService.find("from RoleAuth where roleId ="+role.getId()+" and resourceId ="+resource.getId());
//                            //info.addStringPermission(roleAuth.getActions());
//                        }
//                    }
//                }
//                //将该用户的所有资源进行排序排列
//                Collections.sort(resourceList, new Comparator<Resource>() {
//                    public int compare(Resource o1, Resource o2) {
//                        return o1.getId().compareTo(o2.getId());
//                    }
//
//                });
//                SecurityUtils.getSubject().getSession().setAttribute("resourceList", resourceList);
                return new SimpleAuthenticationInfo(user.getUsername(), user.getPassword(), getName());
            }
        }
        return null;
    }

    public SecurityServices getSecurityService() {
        return securityService;
    }

    public void setSecurityService(SecurityServices securityService) {
        this.securityService = securityService;
    }


}
