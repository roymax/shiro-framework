package com.gzbosoft.security.web.struts2.security;

import com.gzbosoft.framework.dao.Paging;
import com.gzbosoft.framework.web.action.BaseAction;
import com.gzbosoft.framework.web.action.CURDAction;
import com.gzbosoft.security.domain.Resource;
import com.gzbosoft.security.services.SecurityServices;
import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.rest.DefaultHttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by IntelliJ IDEA.
 * User: roymax
 * Date: 12-8-13
 * Time: 下午11:03
 * To change this template use File | Settings | File Templates.
 */
public class ResourceAction extends BaseAction implements ModelDriven<Object> {

    private Resource resource = new Resource();
    private Paging paging = null;

    @Autowired
    private SecurityServices securityServices;


    /**
     * 资源列表页
     *
     * @return
     */
    public String index() {
        paging = securityServices.findResourcesByPaging(0, 10);
        return SUCCESS;
    }


    /**
     * 打开新增资源页
     *
     * @return
     */
    public String editNew() {
        return CREATE;
    }


    /**
     * 创建资源
     *
     * @return
     * @throws Exception
     */
    public String create() throws Exception {
        resource.setStatus("1");
        securityServices.saveResourece(resource);
        addActionMessage("添加资源成功");
        //重定向
//        return new DefaultHttpHeaders(SUCCESS).disableCaching().withNoETag();
        return index();
    }
   

    public String edit() throws Exception {
        return null;
    }

    public String update() throws Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public String show() throws Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public String destroy() throws Exception {
        securityServices.deleteResource(resource.getId());
        addActionMessage("删除资源成功");
        return index();
    }


    public void setResource(Resource resource) {
        this.resource = resource;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }

    public Resource getResource() {
        return resource;
    }

    public Paging getPaging() {
        return paging;
    }

    @Override
    public Object getModel() {
        return paging != null ? paging : resource;
    }
}
