/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gzbosoft.security.web.struts2.security;

import com.gzbosoft.framework.web.action.BaseAction;
import com.gzbosoft.security.domain.User;
import com.gzbosoft.security.services.SecurityServices;
import com.opensymphony.xwork2.ModelDriven;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.apache.struts2.rest.DefaultHttpHeaders;
import org.apache.struts2.rest.HttpHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author roymax
 */
public class LoginAction extends BaseAction implements ModelDriven<Object> {

    private static final Logger log = LoggerFactory.getLogger(LoginAction.class);

    @Autowired
    private SecurityServices securityServices;
    private User user = new User();

    /**
     * 显示登录页
     *
     * @return
     * @throws Exception
     */
    public String index() {
        return SUCCESS;
    }

    /**
     * 提交登录请求
     *
     * @return
     */
    public HttpHeaders create() {
        log.debug("login username: {} \tpassword: {}", user.getUsername(), user.getPassword());
        UsernamePasswordToken token = null;
        token = new UsernamePasswordToken(user.getUsername(), user.getPassword(), false);
        Subject subject = SecurityUtils.getSubject();
        subject.login(token);

        return new DefaultHttpHeaders(HOME).disableCaching().withNoETag();
    }

    public void setUser(User user) {
        this.user = user;
    }
    @Override
    public Object getModel() {
        return user;
    }
}
