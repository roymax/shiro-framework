package com.gzbosoft.security.web.struts2.security;

import com.gzbosoft.framework.dao.Paging;
import com.gzbosoft.framework.web.action.BaseAction;
import com.gzbosoft.security.domain.Resource;
import com.gzbosoft.security.services.SecurityServices;
import com.opensymphony.xwork2.ModelDriven;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by IntelliJ IDEA.
 * User: roymax
 * Date: 12-8-13
 * Time: 下午11:03
 * To change this template use File | Settings | File Templates.
 */
public class GroupAction extends BaseAction implements ModelDriven<Object> {

    private Resource resource = new Resource();
    private Paging paging = null;

    @Autowired
    private SecurityServices securityServices;


    /**
     * 用户组列表页
     *
     * @return
     */
    public String index() {
        paging = securityServices.findResourcesByPaging(0, 10);
        return SUCCESS;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }

    public Resource getResource() {
        return resource;
    }

    public Paging getPaging() {
        return paging;
    }

    @Override
    public Object getModel() {
        return paging != null ? paging : resource;
    }
}
