package com.gzbosoft.security.web.struts2;

import com.gzbosoft.framework.web.action.BaseAction;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Created by IntelliJ IDEA.
 * User: roymax
 * Date: 12-8-5
 * Time: 下午8:57
 * To change this template use File | Settings | File Templates.
 */
public class DashboardAction extends BaseAction {
    private static final Logger log = LoggerFactory.getLogger(DashboardAction.class);

    @RequiresAuthentication
    public String index() throws Exception {
        return SUCCESS;
    }

}
