package com.gzbosoft.security.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


/**
 * @author ligangying ligangying1987@163.com
 * @ClassName: Users
 * @Description: 用户实体类
 * @date 2011-9-26 上午10:03:54
 */
@Entity
@Table(name = "users")
public class User implements Serializable {

    private String address;
    private String age;
    private String birthday;
    private String education;
    private String email;
    private String employedAt;
    private String nickname;
    private Long id;
    private String lastLogoffAt;
    private String lastLogonAt;
    private String lastLogonIP;
    private String username;
    private String marriage;
    private String memo;
    private String realName;
    private String nativePlace;
    private String password;
    private String phone;
    private String position;
    private String createdAt;
    private String sex;
    private String status;
    private String userType;

    //员工工号
    private String jobNumber;

    @Column(name = "job_number", length = 100)
    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    private Set<Group> groups = new HashSet<Group>();

    @Column(name = "address", length = 50)
    public String getAddress() {
        return address;
    }

    @Column(name = "age", length = 20)
    public String getAge() {
        return age;
    }

    @Column(name = "birthday", length = 10)
    public String getBirthday() {
        return birthday;
    }

    @Column(name = "education", length = 20)
    public String getEducation() {
        return education;
    }

    @Column(name = "email", length = 30)
    public String getEmail() {
        return email;
    }

    @Column(name = "employed_at", length = 10)
    public String getEmployedAt() {
        return employedAt;
    }

    @Column(name = "nickname", length = 30)
    public String getNickname() {
        return nickname;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    public Long getId() {
        return id;
    }

    @Column(name = "last_logoff_at", length = 20)
    public String getLastLogoffAt() {
        return lastLogoffAt;
    }

    @Column(name = "last_logon_at", length = 20)
    public String getLastLogonAt() {
        return lastLogonAt;
    }

    @Column(name = "last_logon_ip", length = 20)
    public String getLastLogonIP() {
        return lastLogonIP;
    }

    @Column(name = "username", nullable = false, length = 30)
    public String getUsername() {
        return username;
    }

    @Column(name = "marriage", length = 2)
    public String getMarriage() {
        return marriage;
    }

    @Column(name = "memo", length = 50)
    public String getMemo() {
        return memo;
    }

    @Column(name = "real_name", nullable = false, length = 30)
    public String getRealName() {
        return realName;
    }

    @Column(name = "native_place", length = 30)
    public String getNativePlace() {
        return nativePlace;
    }

    @Column(name = "password", nullable = false, length = 64)
    public String getPassword() {
        return password;
    }

    @Column(name = "phone", length = 30)
    public String getPhone() {
        return phone;
    }

    @Column(name = "position", length = 30)
    public String getPosition() {
        return position;
    }

    @Column(name = "createdAt", length = 20)
    public String getCreatedAt() {
        return createdAt;
    }

    @Column(name = "sex", length = 10)
    public String getSex() {
        return sex;
    }

    @Column(name = "status", length = 20)
    public String getStatus() {
        return status;
    }

    @Column(name = "user_type", length = 2)
    public String getUserType() {
        return userType;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setEmployedAt(String employedAt) {
        this.employedAt = employedAt;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setLastLogoffAt(String lastLogoffAt) {
        this.lastLogoffAt = lastLogoffAt;
    }

    public void setLastLogonAt(String lastLogonAt) {
        this.lastLogonAt = lastLogonAt;
    }

    public void setLastLogonIP(String lastLogonIP) {
        this.lastLogonIP = lastLogonIP;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setMarriage(String marriage) {
        this.marriage = marriage;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public void setNativePlace(String nativePlace) {
        this.nativePlace = nativePlace;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    @ManyToMany(cascade = {CascadeType.MERGE})
    @JoinTable(
            name = "users_groups",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "group_id")}
    )
    public Set<Group> getGroups() {
        return groups;
    }

    public void setGroups(Set<Group> groups) {
        this.groups = groups;
    }


}
