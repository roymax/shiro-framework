package com.gzbosoft.security.domain;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


/**
 * @author sunct sunchaotong18@163.com
 * @ClassName: Resource
 * @Description:资源信息实体类
 * @date 2011-9-27 下午5:55:01
 */
@Entity
@Table(name = "resources")
public class Resource implements java.io.Serializable {

    private Long id;
    private Resource parent;
    private System system;
    private String name;
    private String code;
    private String resourceType;
    private String link;

    private String status;
    private int orderBy;
    private String memo;
    private Set<Resource> children = new HashSet<Resource>(0);
    private Set<Action> actions = new HashSet<Action>(0);


    // Constructors

    /**
     * default constructor
     */
    public Resource() {
    }

    /**
     * minimal constructor
     */
    public Resource(Long id, System system, String name, String code, String status) {
        this.id = id;
        this.system = system;
        this.name = name;
        this.status = status;
        this.code = code;
    }

    // Property accessors
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return this.id;
    }

    /**
     * <p>Title: </p>
     * <p>Description: </p>
     *
     * @param id
     * @param parent
     * @param system
     * @param name
     * @param code
     * @param resourceType
     * @param link
     * @param status
     * @param orderBy
     * @param memo
     * @param children
     * @param actions
     */

    public Resource(Long id, Resource parent, System system, String name,
                    String code, String resourceType, String link, String status,
                    int orderBy, String memo, Set<Resource> children,
                    Set<Action> actions) {
        super();
        this.id = id;
        this.parent = parent;
        this.system = system;
        this.name = name;
        this.code = code;
        this.resourceType = resourceType;
        this.link = link;

        this.status = status;
        this.orderBy = orderBy;
        this.memo = memo;
        this.children = children;
        this.actions = actions;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")

    public Resource getParent() {
        return this.parent;
    }

    public void setParent(Resource parent) {
        this.parent = parent;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "system_id", nullable = false)
    public System getSystem() {
        return this.system;
    }

    public void setSystem(System system) {
        this.system = system;
    }

    @Column(name = "name", nullable = false, length = 30)

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "code", length = 30, nullable = false)

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "resource_type", length = 2)

    public String getResourceType() {
        return this.resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    @Column(name = "link", length = 30)

    public String getLink() {
        return this.link;
    }

    public void setLink(String link) {
        this.link = link;
    }


    @Column(name = "status", nullable = false, length = 30)

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "order_by")

    public int getOrderBy() {
        return this.orderBy;
    }

    public void setOrderBy(int orderBy) {
        this.orderBy = orderBy;
    }

    @Column(name = "memo", length = 50)

    public String getMemo() {
        return this.memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "parent")
    public Set<Resource> getChildren() {
        return this.children;
    }

    public void setChildren(Set<Resource> children) {
        this.children = children;
    }

    /**
     * @return the actions
     */
    @ManyToMany(cascade = {CascadeType.MERGE})
    @JoinTable(
            name = "resources_actions",
            joinColumns = {@JoinColumn(name = "resource_id")},
            inverseJoinColumns = {@JoinColumn(name = "action_id")}
    )
    public Set<Action> getActions() {
        return actions;
    }

    /**
     * @param actions the actions to set
     */
    public void setActions(Set<Action> actions) {
        this.actions = actions;
    }


}