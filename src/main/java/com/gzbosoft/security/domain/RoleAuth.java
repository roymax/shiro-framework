/**   
  * @Title: RoleAuth.java 
  * @Package com.tgyt.permissions.model 
  * @Description: 
  * @author sunct sunchaotong18@163.com 
  * @date 2011-9-30 上午10:30:29 
  * @version V1.0   
  */

package com.gzbosoft.security.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/** 
 * @ClassName: RoleAuth 
 * @Description:角色权限实体
 * @author sunct sunchaotong18@163.com 
 * @date 2011-9-30 上午10:30:29 
 *  
 */
@Entity
@Table(name="roles_auths")
public class RoleAuth {

	private Long id;
	private Long roleId;
	private Long resourceId;
	private String actions;
	
	public RoleAuth() {
		super();
	}
	
	public RoleAuth(Long id, Long roleId, Long resourceId,
			String actions) {
		super();
		this.id = id;
		this.roleId = roleId;
		this.resourceId = resourceId;
		this.actions = actions;
	}
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(name="role_id")
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	@Column(name="resource_id")
	public Long getResourceId() {
		return resourceId;
	}
	public void setResourceId(Long resourceId) {
		this.resourceId = resourceId;
	}
	@Column(name="actions")
	public String getActions() {
		return actions;
	}
	public void setActions(String actions) {
		this.actions = actions;
	}
	
}
