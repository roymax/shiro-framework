package com.gzbosoft.security.domain;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


/**
 * @author sunct sunchaotong18@163.com
 * @ClassName: Actions
 * @Description: 操作管理实体类
 * @date 2011-9-19 下午2:45:45
 */
@Entity
@Table(name = "actions")
public class Action implements java.io.Serializable {


    // Fields    

    private Long id;
    private String name;
    private String code;
    private String methodName;
    private String icon;
    private String status;
    private int orderBy;
    private String memo;
    private Set<Resource> resources = new HashSet<Resource>(
            0);

    // Constructors

    /**
     * default constructor
     */
    public Action() {
    }

    /**
     * minimal constructor
     */
    public Action(String name, String code, String status) {
        this.name = name;
        this.code = code;
        this.status = status;
    }

    /**
     * full constructor
     */
    public Action(String name, String code, String methodName, String icon, String status, int orderBy, String memo) {
        this.name = name;
        this.code = code;
        this.methodName = methodName;
        this.icon = icon;
        this.status = status;
        this.orderBy = orderBy;
        this.memo = memo;
    }


    /**
     * <p>Title: </p>
     * <p>Description: </p>
     *
     * @param id
     * @param name
     * @param code
     * @param methodName
     * @param icon
     * @param status
     * @param orderBy
     * @param memo
     * @param resources
     */

    public Action(Long id, String name, String code, String methodName,
                  String icon, String status, int orderBy, String memo,
                  Set<Resource> resources) {
        super();
        this.id = id;
        this.name = name;
        this.code = code;
        this.methodName = methodName;
        this.icon = icon;
        this.status = status;
        this.orderBy = orderBy;
        this.memo = memo;
        this.resources = resources;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "name", nullable = false, length = 30)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Column(name = "method_name", length = 100)
    public String getMethodName() {
        return this.methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    @Column(name = "icon", length = 60)

    public String getIcon() {
        return this.icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Column(name = "status", nullable = false)

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "order_by")

    public int getOrderBy() {
        return this.orderBy;
    }

    public void setOrderBy(int orderBy) {
        this.orderBy = orderBy;
    }

    @Column(name = "memo", length = 50)

    public String getMemo() {
        return this.memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }



    @Column(name = "code", nullable = false, length = 30)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the resource
     */
    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(
            name = "resources_actions",//中间表名
            joinColumns = {@JoinColumn(name = "action_id")},//设置自己在中间表的对应外键
            inverseJoinColumns = {@JoinColumn(name = "resource_id")}//设置对方()在中间表的对应外键
    )
    public Set<Resource> getResources() {
        return resources;
    }

    public void setResources(Set<Resource> resources) {
        this.resources = resources;
    }
}