/**   
  * @Title: Role.java 
  * @Package com.tgyt.permissions.model 
  * @Description: 
  * @author WangMing wang1988ming@qq.com 
  * @date 2011-9-23 下午1:42:08 
  * @version V1.0   
  */

package com.gzbosoft.security.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/** 
 * @ClassName: Role 
 * @Description: 权限实体类 
 * @author WangMing wang1988ming@qq.com
 * @date 2011-9-23 下午1:42:08 
 *  
 */
@Entity
@Table(name="roles")
public class Role implements java.io.Serializable {

	private static final long serialVersionUID = -5911423238732456148L;
	private Long id;
	private String name;
	private String code;
	private String status;
	private int orderBy;
	private String memo;
	
	private Set<Group> groups = new HashSet<Group>();
	
	private Set<Resource> resources = new HashSet<Resource>();
	@Id
	@Column(name="id",unique=true,nullable=false)
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(name="name",length=30,nullable=false)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column(name="code",length=30)
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	@Column(name="status",nullable=false)
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Column(name="order_by")
	public int getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(int orderBy) {
		this.orderBy = orderBy;
	}
	@Column(name="memo",length=50)
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Role() {
		super();
	}
	
	public Role(Long id, String name, String code, String status,
			int orderBy, String memo) {
		super();
		this.id = id;
		this.name = name;
		this.code = code;
		this.status = status;
		this.orderBy = orderBy;
		this.memo = memo;
	}
	@Override
	public String toString() {
		return "Role [id=" + id + ", name=" + name + ", code=" + code
				+ ", status=" + status + ", orderBy=" + orderBy + ", memo="
				+ memo + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Role other = (Role) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@ManyToMany(cascade=CascadeType.MERGE)
	@JoinTable(
			name="groups_roles",
			joinColumns={@JoinColumn(name="role_id")},
			inverseJoinColumns={@JoinColumn(name="group_id")}
	)
	public Set<Group> getGroups() {
		return groups;
	}
	public void setGroups(Set<Group> groups) {
		this.groups = groups;
	}
	@ManyToMany(cascade=CascadeType.MERGE)
	@JoinTable(
			name="roles_auths",
			joinColumns={@JoinColumn(name="role_id")},
			inverseJoinColumns={@JoinColumn(name="resource_id")}
	)
	public Set<Resource> getResources() {
		return resources;
	}
	public void setResources(Set<Resource> resources) {
		this.resources = resources;
	}
	
	
	
}
