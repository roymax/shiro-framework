package com.gzbosoft.security.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/** 
  * @ClassName: Group
  * @Description: 组实体类
  * @author ligangying ligangying1987@163.com 
  * @date 2011-9-26 上午10:02:29 
  *  
  */
@Entity
@Table(name = "groups")
public class Group implements Serializable {
	private String code;
	private String groupType;
	private Long id;
	private String memo;
	private String name;
	private int orderBy;
	private Long parentId;
	private String status;
	
	private Set<User> users = new HashSet<User>();
	
	private Set<Role> roles = new HashSet<Role>();
	
	@Column(name="code",length=30)
	public String getCode() {
		return code;
	}
	@Column(name="group_type",length=20)
	public String getGroupType() {
		return groupType;
	}
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id",nullable=false,unique=true)
	public Long getId() {
		return id;
	}
	@Column(name="memo")
	public String getMemo() {
		return memo;
	}
	@Column(name="name",nullable=false)
	public String getName() {
		return name;
	}
	@Column(name="order_by")
	public int getOrderBy() {
		return orderBy;
	}
	@Column(name="parent_id")
	public Long getParentId() {
		return parentId;
	}
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setOrderBy(int orderBy) {
		this.orderBy = orderBy;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@ManyToMany(cascade={CascadeType.MERGE})
	@JoinTable(
			name="users_groups",
			joinColumns={@JoinColumn(name="group_id")},
			inverseJoinColumns={@JoinColumn(name="user_id")}
	)
	public Set<User> getUsers() {
		return users;
	}
	
	public void setUsers(Set<User> users) {
		this.users = users;
	}
	@ManyToMany(cascade={CascadeType.MERGE})
	@JoinTable(
			name="groups_roles",
			joinColumns={@JoinColumn(name="group_id")},
			inverseJoinColumns={@JoinColumn(name="role_id")}
	)
	public Set<Role> getRoles() {
		return roles;
	}
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
}
