package com.gzbosoft.security.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author ligangying ligangying1987@163.com
 * @ClassName: System
 * @Description: 系统信息实体类
 * @date 2011-9-26 上午10:03:39
 */
@Entity
@Table(name = "systems")
public class System implements Serializable {

    private String createdAt;
    private String contextPath;
    private String code;
    private String icon;
    private Long id;
    private String logo;
    private String memo;
    private String name;
    private int orderBy;
    private String status;
    private String tablePrefix;
    private String version;

    public System() {
        super();
    }

    public System(Long id, String name, String code, String contextPath,
                  String tablePrefix, String logo, String icon, String version,
                  String createdAt, String status, int orderBy, String memo) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.contextPath = contextPath;
        this.tablePrefix = tablePrefix;
        this.logo = logo;
        this.icon = icon;
        this.version = version;
        this.createdAt = createdAt;
        this.status = status;
        this.orderBy = orderBy;
        this.memo = memo;
    }

    @Column(name = "created_at", length = 10)
    public String getCreatedAt() {
        return createdAt;
    }

    @Column(name = "context_path", length = 50, nullable = false)
    public String getContextPath() {
        return contextPath;
    }

    @Column(name = "code", length = 30)
    public String getCode() {
        return code;
    }

    @Column(name = "icon", length = 30)
    public String getIcon() {
        return icon;
    }

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    @Column(name = "logo", length = 30)
    public String getLogo() {
        return logo;
    }

    @Column(name = "memo", length = 50)
    public String getMemo() {
        return memo;
    }

    @Column(name = "name", nullable = false, length = 30)
    public String getName() {
        return name;
    }

    @Column(name = "order_by", length = 10)
    public int getOrderBy() {
        return orderBy;
    }

    @Column(name = "status", nullable = false, length = 10)
    public String getStatus() {
        return status;
    }

    @Column(name = "table_prefix", nullable = false, length = 50)
    public String getTablePrefix() {
        return tablePrefix;
    }

    @Column(name = "version", length = 10)
    public String getVersion() {
        return version;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOrderBy(int orderBy) {
        this.orderBy = orderBy;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setTablePrefix(String tablePrefix) {
        this.tablePrefix = tablePrefix;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
