package com.gzbosoft.security.services;

import com.gzbosoft.framework.dao.Paging;
import com.gzbosoft.security.domain.Resource;
import com.gzbosoft.security.domain.RoleAuth;
import com.gzbosoft.security.domain.User;
import org.apache.shiro.authz.annotation.RequiresPermissions;

/**
 * Created by IntelliJ IDEA.
 * User: roymax
 * Date: 12-8-5
 * Time: 下午4:44
 * To change this template use File | Settings | File Templates.
 */
public interface SecurityServices {
    /**
     * 分页返回用户列表
     *
     * TODO delete it
     * @param begin
     * @param interval
     * @return
     */

    public Paging findUserByPaging(int begin, int interval);

    /**
     * 登录系统
     * @param username
     * @param password
     */
    public User login(String username , String password);
    
    /**
     * 查找角色权限对象
     * @param roleId
     * @param resourceId
     * @return
     */
    public RoleAuth getByRoleIdAndResourceId(Long roleId, Long resourceId);

    /**
     * 资源分页
     *
     * @param begin
     * @param interval
     * @return
     */
    public Paging findResourcesByPaging(int begin, int interval);

    /**
     * 保存资源对象
     * @param resource
     */
    public void saveResourece(Resource resource);

    /**
     * 删除资源
     * @param id
     */
    public void deleteResource(Long id);
}

