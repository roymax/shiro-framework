package com.gzbosoft.security.services.impl;

import com.gzbosoft.framework.dao.Paging;
import com.gzbosoft.security.dao.LogDao;
import com.gzbosoft.security.domain.Logs;
import com.gzbosoft.security.services.LogServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class LogServicesImpl implements LogServices {
	
	@Autowired
	private LogDao logDao;
	
	public Paging getPageList(Logs log,int page,int rows,String sort,String order){
		return logDao.getPageList(log, page, rows,sort,order);
	}
}
