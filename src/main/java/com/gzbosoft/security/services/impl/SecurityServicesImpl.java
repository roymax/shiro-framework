package com.gzbosoft.security.services.impl;

import com.gzbosoft.framework.dao.Paging;
import com.gzbosoft.security.dao.ResourcesDAO;
import com.gzbosoft.security.dao.RoleAuthDao;
import com.gzbosoft.security.dao.UserDao;
import com.gzbosoft.security.domain.Resource;
import com.gzbosoft.security.domain.RoleAuth;
import com.gzbosoft.security.domain.User;
import com.gzbosoft.security.services.SecurityServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Created by IntelliJ IDEA.
 * User: roymax
 * Date: 12-8-5
 * Time: 下午4:45
 * To change this template use File | Settings | File Templates.
 */
@Service("securityServices")
public class SecurityServicesImpl implements SecurityServices {
    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleAuthDao roleAuthDao;
    @Autowired
    private ResourcesDAO resourcesDAO;


    @Override
    public RoleAuth getByRoleIdAndResourceId(Long roleId, Long resourceId) {
        List roleAuth = roleAuthDao.find("from RoleAuth where roleId = ? and resourceId = ?", roleId, resourceId);
        return roleAuth.size() != 0 ? (RoleAuth) roleAuth.get(0) : null;
    }

    @Override
    public User login(String username, String password) {
        return userDao.getByUsernameAndPassword(username, password);
    }

    @Override
    public Paging findUserByPaging(int begin, int interval) {
        return userDao.pagedQuery("from User", begin, interval);
    }

    @Override
    public Paging findResourcesByPaging(int begin, int interval) {
        return resourcesDAO.pagedQuery("from Resource", begin, interval);
    }

    @Override
    public void saveResourece(Resource resource) {
        resourcesDAO.saveOrUpdate(resource);
    }

    @Override
    public void deleteResource(Long id) {
        resourcesDAO.removeById(id);
    }
}
