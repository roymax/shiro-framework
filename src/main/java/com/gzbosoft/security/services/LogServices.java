package com.gzbosoft.security.services;


import com.gzbosoft.framework.dao.Paging;
import com.gzbosoft.security.domain.Logs;

/**
  * @ClassName: ILogService 
  * @Description: 日志业务层接口
  *
  */
public interface LogServices {
	/** 
	  * @Title: getPageList 
	  * @Description: 获取具体某页的记录
	  * @param @param log 一条具体log对象的信息
	  * @param @param page 当前页
	  * @param @param rows 一页显示的多少记录
	  * @param @return
	  * @return Pagination
	  * @throws 
	  */
	public Paging getPageList(Logs log,int page,int rows,String sort,String order);
}
